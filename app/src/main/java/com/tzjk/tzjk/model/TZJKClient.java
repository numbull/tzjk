package com.tzjk.tzjk.model;

import android.content.Context;
import android.os.Build;

import com.tzjk.tzjk.http.AsyncHttpClient;
import com.tzjk.tzjk.http.AsyncHttpResponseHandler;
import com.tzjk.tzjk.http.RequestParams;
import com.tzjk.tzjk.http.ResposeCallback;
import com.tzjk.tzjk.http.TZJKHttpHandler;


/**
 * Created by wangziren on 15/8/9.
 */
public class TZJKClient {
    public static String URL = "http://192.168.0.18/TestApi/app.php";

    private static AsyncHttpClient client;

    static {
        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(2, 2000);
    }

    public static void getUserInfo(ResposeCallback<Responsemod> callback, RequestParams params, Context context) {
        post(URL, params, new TZJKHttpHandler<Responsemod>(callback, context) {
        }, context);
    }

    /*
    Help methods
     */
    public static void get(String url, RequestParams params, AsyncHttpResponseHandler
            responseHandler) {
        client.setUserAgent(Build.DEVICE + ";" +
                Build.VERSION.RELEASE + ";" + "" + ";" + "");
        client.get(url, params, responseHandler);
    }


    public static void post(String url, RequestParams params, AsyncHttpResponseHandler
            responseHandler, Context context) {
        client.setUserAgent(Build.DEVICE + ";" +
                Build.VERSION.RELEASE + ";" + "" + ";" + "");
        client.post(url, params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler
            responseHandler) {
        client.setUserAgent(Build.DEVICE + ";" +
                Build.VERSION.RELEASE + ";" + "" + ";" + "");
        client.post(url, params, responseHandler);
    }

    public static void put(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.put(url, params, responseHandler);
    }

    public static void delete(String url, AsyncHttpResponseHandler responseHandler) {
        client.delete(url, responseHandler);
    }


}
