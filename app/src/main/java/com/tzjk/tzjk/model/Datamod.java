package com.tzjk.tzjk.model;/**
 * Created by cyril on 16/1/10.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class Datamod {

    @Expose
    @SerializedName("userInfo")
    private UserInfomod userInfo;

    @Expose
    @SerializedName("isLogin")
    private boolean isLogin;

    @Expose
    @SerializedName("unread")
    private String unread;

    @Expose
    @SerializedName("untask")
    private String untask;

    public UserInfomod getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfomod userInfo) {
        this.userInfo = userInfo;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setIsLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }

    public String getUntask() {
        return untask;
    }

    public void setUntask(String untask) {
        this.untask = untask;
    }
}
