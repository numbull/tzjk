package com.tzjk.tzjk.http;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by wangziren on 15/8/8.
 */
public class TZJKHttpHandler<T> extends JsonHttpResponseHandler {
    private ResposeCallback<T> callback;
    private Context context;
    private Gson gson;
    private Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public TZJKHttpHandler(ResposeCallback<T> callback, Context context) {
        this.callback = callback;
        this.context = context;
        this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .enableComplexMapKeySerialization().serializeNulls()
                .setDateFormat("yyyy-MM-dd").setPrettyPrinting()
                .registerTypeAdapter(TipText.class, new TipTextSerializer())
                .registerTypeAdapter(TipText.class, new TipTextDeserializer())
                .create();
        this.entityClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

    }

    public TZJKHttpHandler(ResposeCallback<T> callback) {
        this.callback = callback;
        //this.context = context;
        this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .enableComplexMapKeySerialization().serializeNulls()
                .setDateFormat("yyyy-MM-dd").setPrettyPrinting()
                .registerTypeAdapter(TipText.class, new TipTextSerializer())
                .registerTypeAdapter(TipText.class, new TipTextDeserializer())
                .create();
        this.entityClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        super.onSuccess(statusCode, headers, response);

        try {
            this.callback.onSuccess(parseToType(response));
        } catch (JsonSyntaxException e) {
            this.callback.onFailure(statusCode, headers, e, response);
        }
    }

    protected T parseToType(JSONObject response) throws JsonSyntaxException {
        T t = this.gson.fromJson(response.toString(), this.entityClass);

        return t;
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
        super.onSuccess(statusCode, headers, response);
        try {
            int length = response.length();
            List<T> result = new ArrayList<T>();
            for (int i = 0; i < length; i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                result.add(parseToType(jsonObject));
            }
            this.callback.onSuccess(result);
        } catch (JSONException e) {
            this.callback.onFailure(statusCode, headers, e, response);
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        super.onFailure(statusCode, headers, throwable, errorResponse);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        super.onFailure(statusCode, headers, throwable, errorResponse);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        super.onFailure(statusCode, headers, responseString, throwable);
    }

    @Override
    public void onSuccess(int statusCode, final Header[] headers, String responseString) {
        if (statusCode != HttpStatus.SC_NO_CONTENT) {
            final byte[] responseBytes = responseString.getBytes();
            final int status = statusCode;
            final Header[] head = headers;
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        final Object jsonResponse = parseResponse(responseBytes);
                        postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                if (jsonResponse instanceof JSONObject) {
                                    onSuccess(status, head, (JSONObject) jsonResponse);
                                } else if (jsonResponse instanceof JSONArray) {
                                    onSuccess(status, head, (JSONArray) jsonResponse);
                                } else if (jsonResponse instanceof String) {
                                    onFailure(status, head, (String) jsonResponse,
                                            new JSONException("Response cannot be paresd as Json data"));
                                } else {
                                    onFailure(status, head, new JSONException("Unexpected response type " + jsonResponse.getClass().getName()),
                                            (JSONObject) null);
                                }
                            }
                        });
                    } catch (final JSONException e) {
                        postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                onFailure(status, head, e, (JSONObject) null);
                            }
                        });
                    }
                }
            };
            if (!getUseSynchronousMode()) {
                new Thread(runnable).start();
            } else {
                runnable.run();
            }
        } else {
            onSuccess(statusCode, headers, new JSONObject());
        }
    }
}
