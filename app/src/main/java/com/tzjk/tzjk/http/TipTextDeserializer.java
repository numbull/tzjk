package com.tzjk.tzjk.http;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by wangziren on 15/8/9.
 */
public class TipTextDeserializer implements JsonDeserializer<TipText> {
    @Override
    public TipText deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        TipText tipText = new TipText();
        tipText.setText(jsonElement.getAsJsonPrimitive().getAsString());
        return tipText;
    }
}
