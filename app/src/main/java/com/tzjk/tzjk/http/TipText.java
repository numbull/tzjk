package com.tzjk.tzjk.http;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by wangziren on 15/8/9.
 */
public class TipText implements Parcelable {
    private Long id;
    private String text;
    private Long tipId;

    public TipText(){}

    public TipText(Long id){
        this.id = id;
    }

    public TipText(Long id, String text, Long tipId){
        this.id = id;
        this.text = text;
        this.tipId = tipId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getTipId() {
        return tipId;
    }

    public void setTipId(Long tipId) {
        this.tipId = tipId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(id);
        dest.writeString(text);
        dest.writeSerializable(tipId);
    }

    public static final Creator<TipText> CREATOR = new Creator<TipText>() {
        @Override
        public TipText createFromParcel(Parcel source) {
            Serializable sId = source.readSerializable();
            Long id = SerializableUtils.toLong(sId);
            String text = source.readString();
            Serializable sTipId = source.readSerializable();
            Long tipId = SerializableUtils.toLong(sTipId);
            return new TipText(id, text, tipId);

        }

        @Override
        public TipText[] newArray(int size) {
            return new TipText[size];
        }
    };
}
