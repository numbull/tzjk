package com.tzjk.tzjk.http;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wangziren on 15/8/9.
 */
/*
Use to deserialize the encapsulated type
 */
public class SerializableUtils {
    public static Long toLong(Serializable s) {
        return null == s ? null : (Long) s;
    }

    public static Integer toInteger(Serializable s) {
        return null == s ? null : (Integer) s;
    }

    public static Date toDate(Serializable s) {
        return null == s ? null : (Date) s;
    }

    public static Double toDouble(Serializable s) {
        return null == s ? null : (Double) s;
    }

    public static Boolean toBoolean(Serializable s) {
        return null == s ? null : (Boolean) s;
    }

    public static Float toFloat(Serializable s) {
        return null == s ? null : (Float) s;
    }
}
