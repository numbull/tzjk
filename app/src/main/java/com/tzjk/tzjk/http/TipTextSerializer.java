package com.tzjk.tzjk.http;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by wangziren on 15/8/9.
 */
public class TipTextSerializer implements JsonSerializer<TipText> {
    @Override
    public JsonElement serialize(TipText tipText, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(tipText.getText());
    }
}
