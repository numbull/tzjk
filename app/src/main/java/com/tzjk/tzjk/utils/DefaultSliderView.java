package com.tzjk.tzjk.utils;
/**
 * Created by cyril on 16/2/23.
 * 替换cn.lightsky.infiniteindicator.slideview中的DefaultSliderView
 * 实现加载动画的样式自定义
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.tzjk.tzjk.R;

import cn.lightsky.infiniteindicator.slideview.BaseSliderView;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class DefaultSliderView extends BaseSliderView {
    public DefaultSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.dsv_render_type_default,null);
        ImageView target = (ImageView)v.findViewById(cn.lightsky.infiniteindicator.R.id.slider_image);
        bindEventAndShow(v, target);
        return v;
    }
}
