package com.tzjk.tzjk.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by wangziren on 15/8/6.
 */
public class StringUtils {
    private static String code = "zjmaxJ{q*2!H&akQ$cM|0com";

    //格式化数字字符串
    public static String formatCurrency(String currency) {
        String retStr = "";
        //小数格式化输出
        try {
            if (currency.indexOf(".") != -1) {
                double d = Double.parseDouble(currency);
                NumberFormat numberFormat = NumberFormat.getNumberInstance();
                //设置小数部分最大位数
                numberFormat.setMaximumFractionDigits(2);
                retStr = numberFormat.format(d);
            } else {//长整形格式化输出
                long l = Long.parseLong(currency);
                NumberFormat numberFormat = NumberFormat.getIntegerInstance();
                retStr = numberFormat.format(l);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retStr;
    }

    public static String formatCurrency1(String currency) {
        String retStr = "";
        //小数格式化输出
        try {
            if (currency.indexOf(".") != -1) {
                double d = Double.parseDouble(currency);
                DecimalFormat df = new DecimalFormat(",##0.00");
                //设置小数部分最大位数
                retStr = df.format(d);
            } else {//长整形格式化输出
                long l = Long.parseLong(currency);
                DecimalFormat df = new DecimalFormat(",##0");
                retStr = df.format(l);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retStr;
    }


    public static String getRandomString(int intMin, int intMax) {
        String strResult = "";

        if (intMax < intMin) {
            int temp = intMax;
            intMax = intMin;
            intMin = temp;
        }
        if (intMax == 0) {
            return "最大位数不为正数的字符串无法生成";
        }
        Random r = new Random();
        int randomNum = r.nextInt();
        int randLength = randomNum % (intMax - intMin + 1) + intMin;
        strResult = getRandomStringByLenth(randLength);
        return strResult;

    }

    private static String getRandomStringByLenth(int len) {

        String strResult = "";
        String base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < len; i++) {

            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        strResult = sb.toString();
        return strResult;
    }

    /*
    以344的格式格式化手机号码
     */
    public static String formatPhoneNumber(String input) {

        String result = "";
        char[] stringArr = input.toCharArray();

        int k = 1;
        for (int i = 0; i < stringArr.length; i++) {
            result = result + stringArr[i];
            if (k == 3 || k == 7) {

                result = result + " ";
            }
            k++;
        }

        return result;
    }

    //统一计量单位
    public static String[] formatUnit(String min) {
        String result[] = new String[2];
        try {
            DecimalFormat format = new DecimalFormat("####");
            String minx = format.format(Float.parseFloat(min));
            char[] mincs = minx.toCharArray();
            //因为单位是
            if (mincs.length < 5) {
                result[0] = formatCurrency(Float.parseFloat(minx) + "");
                result[1] = "元";
            } else {
                result[0] = formatCurrency(Float.parseFloat(minx) / 10000 + "");
                result[1] = "万元";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    //隐藏手机号码的中间4位
    public static String Hide4BitLetter(String input) {
        String result = "";
        if (ValidateUtils.isMobilePhone(input)) {
            char[] ch = input.toCharArray();
            for (int index = 0; index < ch.length; index++) {
                if (index >= 3 && index <= 6) {
                    result = result + "*";
                } else {

                    result = result + ch[index];
                }

            }
        } else {

            result = input;
        }

        return result;
    }

    //身份证中间部分隐藏
    public static String HindIDCardMiddlePart(String idCard) {
        String result = "";
        char[] chars = idCard.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i > 3 && i < chars.length - 4) {
                result = result + "*";
            } else {
                result = result + chars[i];
            }
        }
        return result;
    }

    public static String Remain4Lerrers(String input) {
        String result = "";
        char[] chars = input.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i <= chars.length - 5) {
                if (i != 0 && (i + 1) % 4 == 0) {
                    result = result + "* ";
                } else if (i > 11) {
                    result = result + "";
                } else {
                    result = result + "*";
                }
            } else {
                result = result + chars[i];
            }
        }

        return result;
    }

    /**
     * 得到一个字符串的长度,显示的长度,一个汉字或日韩文长度为1,英文字符长度为0.5
     *
     * @param s 需要得到长度的字符串
     * @return int 得到的字符串长度
     */
    public static double getLength(String s) {
        double valueLength = 0;
        String chinese = "[\u4e00-\u9fa5]";
        // 获取字段值的长度，如果含中文字符，则每个中文字符长度为2，否则为1
        for (int i = 0; i < s.length(); i++) {
            // 获取一个字符
            String temp = s.substring(i, i + 1);
            // 判断是否为中文字符
            if (temp.matches(chinese)) {
                // 中文字符长度为1
                valueLength += 1;
            } else {
                // 其他字符长度为1
                valueLength += 1;
            }
        }
        //进位取整
        return Math.ceil(valueLength);
    }

    public static boolean checkName(String name) {
        boolean result = false;
        String regEx = "[\\u4e00-\\u9fa5]+";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(name);
        if (matcher.find() && matcher.group(0).equals(name)) {
            result = true;
        }
        return result;
    }

    public static String HideName(String name) {
        char[] chars = name.toCharArray();
        String result = "";
        for (int i = 0; i < chars.length; i++) {
            if (i < chars.length - 1) {
                result = result + "*";
            } else {
                result = result + chars[i];
            }
        }
        return result;
    }


}
