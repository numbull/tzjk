package com.tzjk.tzjk.utils;/**
 * Created by cyril on 16/3/4.
 */

import android.content.Context;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import android.widget.ImageView;

import com.tzjk.tzjk.R;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class MethodUtils {


    public static  boolean changeVisibility(Context context, boolean isDisplay, EditText editText, ImageView imageView) {
        if (!isDisplay) {
            // display password text, for example "123456"
            editText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            try {
                editText.setSelection(editText.getText().length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.tzjk_icon_eyes_open));
        } else {
            // hide password, display "."
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            try {
                editText.setSelection(editText.getText().length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.tzjk_icon_eyes_close));
        }
        editText.postInvalidate();
        return isDisplay = !isDisplay;
    }
}
