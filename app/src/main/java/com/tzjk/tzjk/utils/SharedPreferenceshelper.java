package com.tzjk.tzjk.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by wangziren on 15/8/9.
 */
/*
使用文本态 保存登陆状态
 */
public class SharedPreferenceshelper {

    //保存数据的方法
    @SuppressLint("CommitPrefEdits")
    public static void SaveSharedPreferences(Context context, String key, String value){
        SharedPreferences sharedPreferences = context.getSharedPreferences("ZJMAX" , Context.MODE_PRIVATE);//私有数据
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    //读取数据的方法
    @SuppressWarnings("deprecation")
    @SuppressLint("WorldReadableFiles")
    public static String ReadSharedPreferences(Context context, String key){
        SharedPreferences sharedPreferences = context.getSharedPreferences("ZJMAX", Activity.MODE_WORLD_READABLE);
        return sharedPreferences.getString(key,"");
    }
}
