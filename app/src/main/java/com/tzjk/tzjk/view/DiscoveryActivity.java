package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/2/21.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.http.RequestParams;
import com.tzjk.tzjk.http.ResposeCallback;
import com.tzjk.tzjk.model.Responsemod;
import com.tzjk.tzjk.model.TZJKClient;
import com.tzjk.tzjk.utils.DefaultSliderView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cn.lightsky.infiniteindicator.InfiniteIndicatorLayout;
import cn.lightsky.infiniteindicator.indicator.CircleIndicator;
import cn.lightsky.infiniteindicator.slideview.BaseSliderView;


/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class DiscoveryActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener {


    @ViewInject(R.id.tzjk_discovery_iil_picture_banner)
    private InfiniteIndicatorLayout infiniteIndicatorLayout;


    /*@OnClick(R.id.button)
    private void BtnClick(View v) {
        OnekeyShare onekeyShare = new OnekeyShare();
        //关闭sso授权
        onekeyShare.disableSSOWhenAuthorize();
        onekeyShare.setSilent(false);
        onekeyShare.setDialogMode();

        onekeyShare.setTitle("台州金控");
        onekeyShare.setText("这是来自台州金控的分享");
        onekeyShare.setUrl("https://www.zjmax.com");
        onekeyShare.setTitleUrl("https://www.zjmax.com");
        onekeyShare.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
        onekeyShare.show(this);
    }
*/
    private String[] response = new String[]{
            "https://raw.githubusercontent.com/lightSky/InfiniteIndicator/master/res/a.jpg",
            "https://raw.githubusercontent.com/lightSky/InfiniteIndicator/master/res/b.jpg",
            "https://raw.githubusercontent.com/lightSky/InfiniteIndicator/master/res/c.jpg",
            "https://raw.githubusercontent.com/lightSky/InfiniteIndicator/master/res/d.jpg"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_discovery_activity);
        ViewUtils.inject(this);
//        getUserInfo();
        for (int i = 0; i < response.length; i++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(DiscoveryActivity.this);
            defaultSliderView.image(response[i])
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(DiscoveryActivity.this)
                    .showImageResForError(R.drawable.tzjk_loading_bg)
                    .showImageResForEmpty(R.drawable.tzjk_loading_bg);

            defaultSliderView.getBundle()
                    .putString("t", "");
            defaultSliderView.getBundle()
                    .putString("id", "");
            defaultSliderView.getBundle()
                    .putString("link", "");
            infiniteIndicatorLayout.addSlider(defaultSliderView);
        }
        infiniteIndicatorLayout.setIndicatorPosition(InfiniteIndicatorLayout.IndicatorPosition.Center_Bottom);
        //设置CircleIndicator的样式
        CircleIndicator circleIndicator = ((CircleIndicator) infiniteIndicatorLayout.getPagerIndicator());
        final float density = getResources().getDisplayMetrics().density;
        circleIndicator.setRadius(5 * density);
        //未选中的背景色
        circleIndicator.setPageColor(getResources().getColor(R.color.tzjk_indicator_color_white));
        //选中的背景色
        circleIndicator.setFillColor(Color.WHITE);
        //边框颜色
        circleIndicator.setStrokeColor(getResources().getColor(R.color.tzjk_indicator_color_white));
        //边框的宽度
        circleIndicator.setStrokeWidth(0 * density);
        infiniteIndicatorLayout.startAutoScroll(3000);
    }

    private void getUserInfo() {
        RequestParams params = new RequestParams();
        params.put("a", "get_users");
        params.put("uid", "10001");
        TZJKClient.getUserInfo(new ResposeCallback<Responsemod>() {
            @Override
            public void onSuccess(Responsemod response) {
//                text.setText(response.getData().getUserInfo().getEmail());
            }

            @Override
            public void onSuccess(List<Responsemod> response) {
                super.onSuccess(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        }, params, this);
    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {
//        String t = baseSliderView.getBundle().get("t") + "";
//        String id = baseSliderView.getBundle().get("id") + "";
        String link = baseSliderView.getBundle().get("link") + "";
//            Intent it = new Intent(DiscoveryActivity.this, WebViewActivity.class);
        if ("".equals(link) || link == null) {
            Toast.makeText(DiscoveryActivity.this, "很抱歉！没有内容", Toast.LENGTH_SHORT).show();
        }
            /* else {
                it.putExtra("link", link);
                it.putExtra("title", " 浙金网");
                PointMallActivity.this.startActivity(it);
            }*/
    }

}



