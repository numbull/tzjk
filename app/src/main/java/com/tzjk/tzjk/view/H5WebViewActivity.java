package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/14.
 */

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.umeng.analytics.MobclickAgent;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */

/*
*
* 用来显示h5相关的信息
 */

public class H5WebViewActivity extends BaseActivity {
    @ViewInject(R.id.tzjk_h5_web_view_title)
    private TextView titleTV;

    @ViewInject(R.id.tzjk_h5_web_view_content_web)
    private WebView contentWeb;

    @OnClick({R.id.toolbar_back_button})
    private void clickListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                if (contentWeb != null && contentWeb.canGoBack()) {
                    contentWeb.goBack();
                } else {
                    H5WebViewActivity.this.finish();
                }
                break;
            default:
                break;
        }
    }

    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_h5_webview_activity);
        ViewUtils.inject(this);

        if (getIntent() != null) {
            title = getIntent().getStringExtra("title");
        }
        titleTV.setText(title);
        contentWeb.loadUrl("http://www.baidu.com");
        contentWeb.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (contentWeb != null && contentWeb.canGoBack()) {
            contentWeb.goBack();
        } else {
            H5WebViewActivity.this.finish();
        }
    }
}
