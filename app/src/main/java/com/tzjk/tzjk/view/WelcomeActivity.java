package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/2/21.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class WelcomeActivity extends BaseActivity {

    @ViewInject(R.id.act_welcome_pager)
    private ViewPager viewPager;
    @ViewInject(R.id.act_welcome_btn_start)
    private Button startBtn;

    @OnClick(R.id.act_welcome_btn_start)
    private void startBtnClicked(View view) {
        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
        WelcomeActivity.this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.tzjk_welcome_activity);
        ViewUtils.inject(this);

        startBtn.setVisibility(View.INVISIBLE);
        List<View> pagerViews = new ArrayList<>();
        View view1 = View.inflate(WelcomeActivity.this, R.layout.tzjk_welcome_pager_a, null);
        View view2 = View.inflate(WelcomeActivity.this, R.layout.tzjk_welcome_pager_b, null);
        View view3 = View.inflate(WelcomeActivity.this, R.layout.tzjk_welcome_pager_c, null);
        View view4 = View.inflate(WelcomeActivity.this, R.layout.tzjk_welcome_pager_d, null);
        View view5 = View.inflate(WelcomeActivity.this, R.layout.tzjk_welcome_pager_e, null);

        pagerViews.add(view1);
        pagerViews.add(view2);
        pagerViews.add(view3);
        pagerViews.add(view4);
        pagerViews.add(view5);


        viewPager.setAdapter(new PagerAdapter(pagerViews));
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        startBtn.setVisibility(View.GONE);
                        break;
                    case 1:
                        startBtn.setVisibility(View.GONE);
                        break;
                    case 2:
                        startBtn.setVisibility(View.GONE);
                        break;
                    case 3:
                        startBtn.setVisibility(View.GONE);
                        break;
                    case 4:
                        startBtn.setVisibility(View.VISIBLE);

                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(WelcomeActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(WelcomeActivity.this);
    }

    /**
     * Created by wangziren on 15/8/4.
     */
    class PagerAdapter extends android.support.v4.view.PagerAdapter {

        private List<View> views;

        public PagerAdapter(List<View> views) {
            this.views = views;
        }

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(View container, int position) {
            ((ViewPager) container).addView(views.get(position), 0);
            return views.get(position);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView(views.get(position));
        }
    }
}

