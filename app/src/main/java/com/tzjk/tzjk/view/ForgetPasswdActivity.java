package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/4.
 */

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.model.CommonConstants;
import com.tzjk.tzjk.utils.ClearEditText;
import com.tzjk.tzjk.utils.MethodUtils;
import com.tzjk.tzjk.utils.ValidateUtils;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class ForgetPasswdActivity extends BaseActivity {

    private boolean isDisplay;
    private boolean running;
    private int count = 60;
    private MyHandler handler;

    @ViewInject(R.id.tzjk_forget_passwd_phone_number)
    private TextView phoneNumberTV;

    @ViewInject(R.id.tzjk_forget_passwd_auth_code_edit_text)
    private EditText authCodeET;

    @ViewInject(R.id.tzjk_forget_passwd_auth_code_send_tv)
    private TextView authCodeSendBtn;

    @ViewInject(R.id.tzjk_forget_passwd_clear_edit_view)
    private ClearEditText passwdCET;

    @ViewInject(R.id.tzjk_forget_passwd_ok_button)
    private Button okBtn;

    @ViewInject(R.id.tzjk_forget_passwd_visible_image_view)
    private ImageView visibleIV;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_forget_passwd_visible_image_view, R.id.tzjk_forget_passwd_auth_code_send_tv, R.id.tzjk_forget_passwd_ok_button})
    private void BtnClickListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                ForgetPasswdActivity.this.finish();
                break;
            case R.id.tzjk_forget_passwd_visible_image_view:
                isDisplay = MethodUtils.changeVisibility(ForgetPasswdActivity.this, isDisplay, passwdCET, visibleIV);
                break;
            case R.id.tzjk_forget_passwd_auth_code_send_tv:
                if (!"".equals(phoneNumberTV.getText().toString())) {
                    if (running) {

                    } else {
                        if (!ValidateUtils.isNetworkAvailable(ForgetPasswdActivity.this)) {
                            Toast.makeText(ForgetPasswdActivity.this, "当前网络不可用", Toast.LENGTH_SHORT).show();
                        } else {
                            sendAuthCode();
                        }
                    }
                } else {
                    Toast.makeText(ForgetPasswdActivity.this, "请先填写手机号", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tzjk_forget_passwd_ok_button:
                if (!ValidateUtils.isPassword(passwdCET.getText().toString())) {
                    Toast.makeText(ForgetPasswdActivity.this, "密码格式不合法", Toast.LENGTH_SHORT).show();
                } else if (authCodeET.getText().toString().length() < 6) {
                    Toast.makeText(ForgetPasswdActivity.this, "请输入正确的验证码", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ForgetPasswdActivity.this, "修改请求已发出", Toast.LENGTH_SHORT).show();
//                    doModify();
                }
                break;
            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_forget_password_activity);
        ViewUtils.inject(this);

        phoneNumberTV.setText(CommonConstants.PHONE_NUMBER);
        handler = new MyHandler(this);

        okBtn.setEnabled(false);
        okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
        authCodeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(authCodeET.getText().toString()) || TextUtils.isEmpty(passwdCET.getText().toString())) {
                    okBtn.setEnabled(false);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                } else {
                    okBtn.setEnabled(true);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwdCET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(authCodeET.getText().toString()) || TextUtils.isEmpty(passwdCET.getText().toString())) {
                    okBtn.setEnabled(false);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                } else {
                    okBtn.setEnabled(true);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    //时间刷新处理
    private class MyHandler extends Handler {
        WeakReference<ForgetPasswdActivity> mActivity;

        public MyHandler(ForgetPasswdActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final ForgetPasswdActivity userForgetPasswdAct = mActivity.get();
            switch (msg.what) {
                case 0:
                    userForgetPasswdAct.authCodeSendBtn.setText("重新发送");
                    userForgetPasswdAct.count = 60;
                    break;
                case 1:
                    userForgetPasswdAct.authCodeSendBtn.setText(userForgetPasswdAct.count + "秒后重发");
                    break;
            }
            super.handleMessage(msg);
        }
    }

    private void sendAuthCode() {
        final Runnable r = new Runnable() {
            public void run() {
                running = true;
                while (count > 0) {
                    count = count - 1;
                    handler.sendEmptyMessage(1);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                running = false;
                handler.sendEmptyMessage(0);
            }
        };
        Thread s = new Thread(r);
        s.start();
    }
}
