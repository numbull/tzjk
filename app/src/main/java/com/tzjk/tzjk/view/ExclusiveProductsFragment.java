package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/16.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.utils.ClearEditText;
import com.tzjk.tzjk.utils.MethodUtils;
import com.tzjk.tzjk.utils.ValidateUtils;

import org.simple.eventbus.EventBus;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */

/*
 *专属产品
 */

public class ExclusiveProductsFragment extends Fragment {

    private View rootView;

    private boolean isDisplay;
    private boolean isVisible = false;//用于对话框的密码 是否可见
    private EventBus eventBus = EventBus.getDefault();

    @ViewInject(R.id.tzjk_exclusive_product_button_layout)
    private LinearLayout buttonLayout;
    @ViewInject(R.id.tzjk_exclusive_product_passwd_cet)
    private ClearEditText passwdCET;
    @ViewInject(R.id.tzjk_exclusive_product_passwd_visible_button_iv)
    private ImageView visibleIV;
    @ViewInject(R.id.tzjk_exclusive_product_check_layout)
    private LinearLayout checkLayout;
    @ViewInject(R.id.tzjk_exclusive_product_introduce_image)
    private ImageView introduceIV;
    @ViewInject(R.id.tzjk_exclusive_product_purchase_button)
    private Button purchaseBtn;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_exclusive_product_passwd_visible_button_ll, R.id.tzjk_exclusive_product_introduce_image,
            R.id.tzjk_exclusive_product_purchase_button, R.id.tzjk_exclusive_product_check_layout})
    private void clickListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                eventBus.post("PRODUCT");
                break;
            case R.id.tzjk_exclusive_product_passwd_visible_button_ll:
                isDisplay = MethodUtils.changeVisibility(rootView.getContext(), isDisplay, passwdCET, visibleIV);
                break;
            case R.id.tzjk_exclusive_product_introduce_image:
                Intent intent = new Intent(getActivity(), H5WebViewActivity.class);
                startActivity(intent);
                break;
            case R.id.tzjk_exclusive_product_purchase_button:
                if (ValidateUtils.isPassword(passwdCET.getText().toString())) {
                    Toast.makeText(getContext(), "我要购买专属产品", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "密码不合法", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tzjk_exclusive_product_check_layout:
                showCheckDialog();
                break;
            default:
                break;
        }
    }

    public static ExclusiveProductsFragment getInstance() {
        ExclusiveProductsFragment exclusiveProductsFragment = new ExclusiveProductsFragment();
        return exclusiveProductsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.tzjk_exclusive_product_fragment, container, false);
            ViewUtils.inject(this, rootView);
            purchaseBtn.setEnabled(false);
            purchaseBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
            passwdCET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (TextUtils.isEmpty(passwdCET.getText().toString())) {
                        purchaseBtn.setEnabled(false);
                        purchaseBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                    } else {
                        purchaseBtn.setEnabled(true);
                        purchaseBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
        eventBus.register(getActivity());
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }
        return rootView;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        eventBus.unregister(getActivity());
    }

    private void showCheckDialog() {

        final MaterialDialog dialog = new MaterialDialog.Builder(getContext())
                .theme(Theme.LIGHT)
                .cancelable(false)
                .customView(R.layout.tzjk_dialog_check_directed_passwd, true)
                .build();

        final ClearEditText passwdCET = (ClearEditText) dialog.findViewById(R.id.tzjk_dialog_check_directed_passwd_cet);
        LinearLayout visibleLL = (LinearLayout) dialog.findViewById(R.id.tzjk_dialog_check_directed_passwd_visible_button_ll);
        final ImageView visibleIV = (ImageView) dialog.findViewById(R.id.tzjk_dialog_check_directed_passwd_visible_button_iv);
        Button cancelBtn = (Button) dialog.findViewById(R.id.tzjk_dialog_check_directed_passwd_cancel_btn);
        Button okBtn = (Button) dialog.findViewById(R.id.tzjk_dialog_check_directed_passwd_ok_btn);

        visibleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isVisible = MethodUtils.changeVisibility(dialog.getContext(), isVisible, passwdCET, visibleIV);
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ValidateUtils.isPassword(passwdCET.getText().toString())) {
                    Toast.makeText(getActivity(), "验证定向密码", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(getActivity(), "定向密码不合法", Toast.LENGTH_SHORT).show();
                }


            }
        });
        dialog.show();
        Display d = getActivity().getWindowManager().getDefaultDisplay();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = (int) (d.getWidth() * 0.8);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(params);
    }

}
