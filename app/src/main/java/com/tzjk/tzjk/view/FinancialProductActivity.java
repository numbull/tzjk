package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/16.
 */

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.tzjk.tzjk.R;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class FinancialProductActivity extends BaseActivity {


    @ViewInject(R.id.tzjk_financial_product_fl)
    private FrameLayout frameLayout;

    private FragmentManager manager;
    private FragmentTransaction transaction;
    private ExclusiveProductsFragment exclusiveProductsFragment;
    private ProductFragment productFragment;

    private EventBus eventBus = EventBus.getDefault();
    private final String INDEX = "PRODUCT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_financial_product_activity);
        ViewUtils.inject(this);
        eventBus.register(this);
        manager = getSupportFragmentManager();
        //设置初始fragment
       getBusMsg(INDEX);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Subscriber
    private void getBusMsg(String msg) {
        if (INDEX.equals(msg)) {
            if (productFragment == null) {
                productFragment = ProductFragment.getInstance();
            }
            transaction = manager.beginTransaction();
            transaction.replace(R.id.tzjk_financial_product_fl, productFragment);
        } else {
            if (exclusiveProductsFragment == null) {
                exclusiveProductsFragment = ExclusiveProductsFragment.getInstance();
            }
            transaction = manager.beginTransaction();
            transaction.replace(R.id.tzjk_financial_product_fl, exclusiveProductsFragment);
        }


        try {
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }
}
