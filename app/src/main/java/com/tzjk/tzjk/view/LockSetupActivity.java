package com.tzjk.tzjk.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.common.RequestInstance;
import com.tzjk.tzjk.utils.LockPatternView;
import com.tzjk.tzjk.utils.LockPatternView.Cell;
import com.tzjk.tzjk.utils.LockPatternView.DisplayMode;

import java.util.ArrayList;
import java.util.List;



/*
 * Author: Ruils 心怀产品梦的安卓码农
 */

public class LockSetupActivity extends BaseActivity implements
        LockPatternView.OnPatternListener, OnClickListener {

    private static final String TAG = "LockSetupActivity";
    @ViewInject(R.id.lock_pattern)
    private LockPatternView lockPatternView;

    @ViewInject(R.id.toolbar_back_hand_passwd_tv)
    private TextView toolbarbackhandpasswdTv;

    @ViewInject(R.id.hand_passwd_tv_title)
    private TextView handpasswdtitleTv;

    @ViewInject(R.id.please_reset_tv)
    private TextView pleaseresetTv;

    private static final int STEP_1 = 1; // 开始
    private static final int STEP_2 = 2; // 第一次设置手势完成
    private static final int STEP_3 = 3; // 按下继续按钮
    private static final int STEP_4 = 4; // 第二次设置手势完成
    private String openpasswd;

    private int step;

    private List<Cell> choosePattern;

    private boolean confirm = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_lock_screen_setup_activity);
        ViewUtils.inject(this);
        if (getIntent() != null) {
            openpasswd = getIntent().getStringExtra("openpasswd");
        }
        toolbarbackhandpasswdTv.setOnClickListener(this);
        pleaseresetTv.setOnClickListener(this);
        lockPatternView.setOnPatternListener(this);
        step = STEP_1;
        updateView();
    }

    //绘制手势四个步骤
    private void updateView() {
        switch (step) {
            case STEP_1:
                choosePattern = null;
                confirm = false;
                lockPatternView.clearPattern();
                lockPatternView.enableInput();
                break;
            case STEP_2:
                step = STEP_3;
                updateView();
                break;
            case STEP_3:
                handpasswdtitleTv.setText("请再次绘制手势密码");
                lockPatternView.clearPattern();
                lockPatternView.enableInput();
                pleaseresetTv.setVisibility(View.VISIBLE);
                break;
            case STEP_4:
                if (confirm) {

                    SharedPreferences preferences = getSharedPreferences(
                            PersonInfoActivity.LOCK, MODE_PRIVATE);
                    preferences
                            .edit()
                            .putString(PersonInfoActivity.LOCK_KEY,
                                    LockPatternView.patternToString(choosePattern))
                            .commit();
                    RequestInstance.count = 5;
                    Toast.makeText(this, "设置成功", Toast.LENGTH_SHORT).show();
                    finish();
                    lockPatternView.disableInput();
                } else {
                    lockPatternView.setDisplayMode(DisplayMode.Wrong);
                    handpasswdtitleTv.setText("与上一次绘制不一致，请重新绘制");
                    lockPatternView.clearPattern();
                    lockPatternView.enableInput();
                }

                break;

            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.toolbar_back_hand_passwd_tv:
                LockSetupActivity.this.finish();
                break;
            case R.id.please_reset_tv:
                handpasswdtitleTv.setText("为了您的账户安全，请绘制手势密码");
                pleaseresetTv.setVisibility(View.GONE);
                step = STEP_1;
                updateView();

            default:
                break;
        }

    }


    @Override
    public void onPatternStart() {
    }

    @Override
    public void onPatternCleared() {
    }

    @Override
    public void onPatternCellAdded(List<Cell> pattern) {
    }

    @Override
    public void onPatternDetected(List<Cell> pattern) {
        //初次绘制少于四个点的处理
        if (pattern.size() < LockPatternView.MIN_LOCK_PATTERN_SIZE) {
            handpasswdtitleTv.setText("请绘制手势密码，至少连接四个点");
            lockPatternView.setDisplayMode(DisplayMode.Wrong);
            lockPatternView.clearPattern();
            return;
        }


        if (choosePattern == null) {
            choosePattern = new ArrayList<Cell>(pattern);
            step = STEP_2;
            updateView();
            return;
        }
        if (choosePattern.equals(pattern)) {

            confirm = true;
        } else {
            confirm = false;
        }
        step = STEP_4;
        updateView();
    }
}
