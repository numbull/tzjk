package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/14.
 */

import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.umeng.analytics.MobclickAgent;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */

/*
*用来显示我的项目中-转让产品的详情
*
*
 */

public class InvestDetailForTransferAct extends BaseActivity {
    @ViewInject(R.id.tzjk_invest_detail_for_transfer_project_name_tv)
    private TextView projectNameTV;
    @ViewInject(R.id.tzjk_invest_detail_for_transfer_web_view)
    private WebView detailWebView;
    @ViewInject(R.id.tzjk_invest_detail_for_transfer_cancel_layout)
    private LinearLayout cancelLayout;
    @ViewInject(R.id.tzjk_invest_detail_for_transfer_more_layout)
    private LinearLayout moreLayout;
    @ViewInject(R.id.tzjk_invest_detail_for_transfer_more_btn)
    private Button moreBtn;
    @ViewInject(R.id.tzjk_invest_detail_for_transfer_cancel_btn)
    private Button cancelBtn;


    @OnClick({R.id.toolbar_back_button, R.id.tzjk_invest_detail_for_transfer_cancel_btn, R.id.tzjk_invest_detail_for_transfer_cancel_layout,
            R.id.tzjk_invest_detail_for_transfer_more_layout, R.id.tzjk_invest_detail_for_transfer_more_btn})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                InvestDetailForTransferAct.this.finish();
                break;
            case R.id.tzjk_invest_detail_for_transfer_cancel_btn:
                showTipsDialog();
                break;
            case R.id.tzjk_invest_detail_for_transfer_more_layout:
                break;
            case R.id.tzjk_invest_detail_for_transfer_more_btn:
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_invest_detail_for_transfer_activity);
        ViewUtils.inject(this);
        detailWebView.loadUrl("http://www.baidu.com");
        detailWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    private void showTipsDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .cancelable(false)
                .customView(R.layout.tzjk_dialog_cancel_transfer, true)
                .build();

        Button cancelBtn = (Button) dialog.findViewById(R.id.tzjk_dialog_cancel_transfer_cancel_btn);
        Button okBtn = (Button) dialog.findViewById(R.id.tzjk_dialog_cancel_transfer_ok_btn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(InvestDetailForTransferAct.this, "撤销转让项目", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
        Display d = getWindowManager().getDefaultDisplay();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = (int) (d.getWidth() * 0.95);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(params);
    }
}
