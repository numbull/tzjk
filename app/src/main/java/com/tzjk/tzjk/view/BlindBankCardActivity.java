package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/11.
 */

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.utils.ValidateUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class BlindBankCardActivity extends BaseActivity {
    @ViewInject(R.id.tzjk_blind_bank_card_number_ed)
    private EditText cardNumberED;
    @ViewInject(R.id.tzjk_blind_bank_card_phone_number_ed)
    private EditText phoneNumberED;
    @ViewInject(R.id.tzjk_blind_bank_card_ok_button)
    private Button okBtn;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_blind_bank_card_ok_button, R.id.tzjk_blind_bank_card_support_bank_tv})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                BlindBankCardActivity.this.finish();
                break;
            case R.id.tzjk_blind_bank_card_ok_button:
                if (cardNumberED.getText().toString().length() != 16) {
                    Toast.makeText(BlindBankCardActivity.this, "您输入的银行卡不合法", Toast.LENGTH_SHORT).show();
                } else {
                    if (ValidateUtils.isMobilePhone(phoneNumberED.getText().toString())) {
                        Toast.makeText(BlindBankCardActivity.this, "请求已提交", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(BlindBankCardActivity.this, "您输入的手机号不合法", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.tzjk_blind_bank_card_support_bank_tv:
                Toast.makeText(BlindBankCardActivity.this, "查询支持的银行", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_blind_bank_card_activity);
        ViewUtils.inject(this);

        okBtn.setEnabled(false);
        okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
        cardNumberED.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(cardNumberED.getText().toString()) && !TextUtils.isEmpty(phoneNumberED.getText().toString())) {
                    okBtn.setEnabled(true);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                } else {
                    okBtn.setEnabled(false);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        phoneNumberED.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(cardNumberED.getText().toString()) && !TextUtils.isEmpty(phoneNumberED.getText().toString())) {
                    okBtn.setEnabled(true);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                } else {
                    okBtn.setEnabled(false);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
