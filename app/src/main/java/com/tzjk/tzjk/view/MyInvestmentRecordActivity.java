package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/11.
 */

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class MyInvestmentRecordActivity extends BaseActivity {

    private List<Fragment> fragmentLists = null;
    private HoldingFragment holdingFragment = null;
    private SettledFragment settledFragment = null;
    private TransferredFragment transferredFragment = null;

    @ViewInject(R.id.tzjk_my_investment_record_holding)
    private LinearLayout holdingLL;
    @ViewInject(R.id.tzjk_my_investment_record_holding_tv)
    private TextView holdingTV;
    @ViewInject(R.id.tzjk_my_investment_record_holding_tag)
    private View holdingView;
    @ViewInject(R.id.tzjk_my_investment_record_settled)
    private LinearLayout settledLL;
    @ViewInject(R.id.tzjk_my_investment_record_settled_tv)
    private TextView settledTV;
    @ViewInject(R.id.tzjk_my_investment_record_settled_tag)
    private View settledView;
    @ViewInject(R.id.tzjk_my_investment_record_transferred)
    private LinearLayout transferredLL;
    @ViewInject(R.id.tzjk_my_investment_record_transferred_tv)
    private TextView transferredTV;
    @ViewInject(R.id.tzjk_my_investment_record_transferred_tag)
    private View transferredView;
    @ViewInject(R.id.tzjk_my_investment_record_viewpager)
    private ViewPager viewPager;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_my_investment_record_question_mark, R.id.tzjk_my_investment_record_holding,
            R.id.tzjk_my_investment_record_settled, R.id.tzjk_my_investment_record_transferred})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                MyInvestmentRecordActivity.this.finish();
                break;
            case R.id.tzjk_my_investment_record_question_mark:
                Intent intent = new Intent(MyInvestmentRecordActivity.this, H5WebViewActivity.class);
                intent.putExtra("title", "投资状态说明");
                startActivity(intent);
                break;
            case R.id.tzjk_my_investment_record_holding:
                changeTab(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.tzjk_my_investment_record_settled:
                changeTab(1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.tzjk_my_investment_record_transferred:
                changeTab(2);
                viewPager.setCurrentItem(2);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_my_investment_record_activity);
        ViewUtils.inject(this);
        fragmentLists = new ArrayList<Fragment>();
        fragmentLists.add(holdingFragment);
        fragmentLists.add(settledFragment);
        fragmentLists.add(transferredFragment);

        holdingTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
        holdingView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(new Adapter(getSupportFragmentManager(), fragmentLists));
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        changeTab(0);
                        break;
                    case 1:
                        changeTab(1);
                        break;
                    case 2:
                        changeTab(2);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    class Adapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentList;

        public Adapter(FragmentManager fm, List<Fragment> list) {
            super(fm);
            this.fragmentList = list;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                if (holdingFragment == null) {
                    holdingFragment = HoldingFragment.newInstance();
                }
                return holdingFragment;
            } else if (position == 1) {
                if (settledFragment == null) {
                    settledFragment = SettledFragment.newInstance();
                }
                return settledFragment;
            } else if (position == 2) {
                if (transferredFragment == null) {
                    transferredFragment = TransferredFragment.newInstance();
                }
                return transferredFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }

    public void changeTab(int position) {
        switch (position) {
            case 0:
                holdingTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
                settledTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                transferredTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                holdingView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
                settledView.setBackgroundColor(Color.WHITE);
                transferredView.setBackgroundColor(Color.WHITE);
                viewPager.setCurrentItem(0);
                break;

            case 1:
                holdingTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                settledTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
                transferredTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                holdingView.setBackgroundColor(Color.WHITE);
                settledView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
                transferredView.setBackgroundColor(Color.WHITE);
                viewPager.setCurrentItem(1);
                break;

            case 2:
                holdingTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                settledTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                transferredTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
                holdingView.setBackgroundColor(Color.WHITE);
                settledView.setBackgroundColor(Color.WHITE);
                transferredView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
                viewPager.setCurrentItem(2);
                break;
        }
    }
}
