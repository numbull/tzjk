package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/14.
 */

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.umeng.analytics.MobclickAgent;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */

/*
*
* 用来显示我的项目-持有中产品的信息
* 支付
* 转让
* 更多项目信息
 */

public class InvestDetailForHoldingAct extends BaseActivity {

    @ViewInject(R.id.tzjk_invest_detail_for_holding_web_view)
    private WebView contentWeb;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_project_name_tv)
    private TextView projectNameTV;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_more_info_layout)
    private LinearLayout moreInfoLayout;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_pay_layout)
    private LinearLayout payLayout;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_pay_btn)
    private Button payBtn;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_cancel_order_btn)
    private Button cancelorderBtn;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_transfer_layout)
    private LinearLayout transferLayout;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_transfer_btn)
    private Button transferBtn;
    @ViewInject(R.id.tzjk_invest_detail_for_holding_more_info_btn)
    private Button moreInfoBtn;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_invest_detail_for_holding_pay_btn, R.id.tzjk_invest_detail_for_holding_cancel_order_btn,
            R.id.tzjk_invest_detail_for_holding_transfer_btn, R.id.tzjk_invest_detail_for_holding_more_info_btn, R.id.tzjk_invest_detail_for_holding_more_info_layout})
    private void clickListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                InvestDetailForHoldingAct.this.finish();
                break;
            case R.id.tzjk_invest_detail_for_holding_pay_btn:
                Toast.makeText(InvestDetailForHoldingAct.this, "支付", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tzjk_invest_detail_for_holding_cancel_order_btn:
                Toast.makeText(InvestDetailForHoldingAct.this, "取消订单", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tzjk_invest_detail_for_holding_transfer_btn:
                Toast.makeText(InvestDetailForHoldingAct.this, "我要转让", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tzjk_invest_detail_for_holding_more_info_btn:
                Toast.makeText(InvestDetailForHoldingAct.this, "更多项目详情", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tzjk_invest_detail_for_holding_more_info_layout:
                Toast.makeText(InvestDetailForHoldingAct.this, "更多项目信息", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    private String status;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_invest_detail_for_holding_activity);
        ViewUtils.inject(this);

        if (getIntent() != null) {
            status = getIntent().getStringExtra("status");
        }
        if ("0".equals(status)) {
            moreInfoLayout.setVisibility(View.VISIBLE);
            payLayout.setVisibility(View.GONE);
            transferLayout.setVisibility(View.GONE);
        } else if ("1".equals(status)) {
            moreInfoLayout.setVisibility(View.GONE);
            payLayout.setVisibility(View.VISIBLE);
            transferLayout.setVisibility(View.GONE);
        } else if ("2".equals(status)) {
            moreInfoLayout.setVisibility(View.GONE);
            payLayout.setVisibility(View.GONE);
            transferLayout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
