package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/2/23.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.tzjk.tzjk.R;

import java.util.ArrayList;
import java.util.List;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class ProductFragment extends Fragment {
    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    @ViewInject(R.id.vp_list_viewpager)
    private ViewPager viewPager;
    @ViewInject(R.id.rb_product_list)
    private RadioButton productRbt;
    @ViewInject(R.id.rb_transferred_list)
    private RadioButton transferredRbt;
    @ViewInject(R.id.header_rgbtn_selector)
    private RadioGroup rgBtnSelector;


    private View rootView;

    public static ProductFragment getInstance() {
        ProductFragment productFragment = new ProductFragment();
        return productFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.tzjk_product_activity, container, false);
            ViewUtils.inject(this, rootView);
            initView();
        }
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }
        return rootView;
    }


    private void initView() {
        fragmentList.add(new ProductListFragment());
        fragmentList.add(new TransferredListFragment());

        ProductFragmentApdater adapter = new ProductFragmentApdater(getChildFragmentManager(), fragmentList);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changeStatus(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        rgBtnSelector.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_product_list:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.rb_transferred_list:
                        viewPager.setCurrentItem(1);
                        break;

                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        rootView = null;

    }

    private void changeStatus(int type) {
        productRbt.setChecked(false);
        transferredRbt.setChecked(false);

        if (type == 0) {
            productRbt.setChecked(true);
        } else if (type == 1) {
            transferredRbt.setChecked(true);
        }
    }

    public class ProductFragmentApdater extends FragmentPagerAdapter {
        private List<Fragment> fragmentList;

        public ProductFragmentApdater(FragmentManager fm, List<Fragment> fragmentList) {
            super(fm);
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList == null ? 0 : fragmentList.size();
        }


    }

}
