package com.tzjk.tzjk.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.common.RequestInstance;
import com.tzjk.tzjk.utils.LockPatternView;
import com.tzjk.tzjk.utils.LockPatternView.Cell;
import com.umeng.analytics.MobclickAgent;

import java.util.List;


/*
 * Author: Ruils 心怀产品梦的安卓码农
 */

public class LockActivity extends BaseActivity implements
        LockPatternView.OnPatternListener {
    private static final String TAG = "LockActivity";
    private String comefrom;

    private String u;
    public static final String COUNT = "count";
    public static final String COUNT_KEY = "count_key";

    private List<Cell> lockPattern;

    @ViewInject(R.id.lock_pattern)
    private LockPatternView lockPatternView;

    @ViewInject(R.id.forgot_passwd_one_tv)
    private TextView forgotpasswdoneTv;

    @ViewInject(R.id.forgot_passwd_two_tv)
    private TextView forgotpasswdtwoTv;

    @ViewInject(R.id.relogin_tv)
    private TextView reloginTv;

    @ViewInject(R.id.hello_title_tv)
    private TextView hellotitleTv;

    @ViewInject(R.id.tell_title_tv)
    private TextView telltitleTv;

    @ViewInject(R.id.act_open_passwd_name)
    private TextView actopenpasswdnameTv;

    @ViewInject(R.id.toolbar_back_hand_passwd_button)
    private TextView toolbarbackhandpasswdTv;


    @OnClick({R.id.toolbar_back_hand_passwd_button, R.id.forgot_passwd_one_tv, R.id.forgot_passwd_two_tv, R.id.relogin_tv})
    private void textviewClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_hand_passwd_button:
                if ("personinfoset".equals(comefrom) || "personinforeset".equals(comefrom) || "personinfoclose".equals(comefrom)) {
                    this.finish();
                } else if ("oncreate".equals(comefrom)) {
                    this.finish();
                    MobclickAgent.onKillProcess(MainActivity.mainActivity);
                    MainActivity.mainActivity.finish();
                    android.os.Process.killProcess(android.os.Process.myPid());
                } else {

                }
                break;
            case R.id.forgot_passwd_one_tv:
                showDailog("忘记密码，您需要重新登录");
                break;
            case R.id.forgot_passwd_two_tv:
                showDailog("忘记密码，您需要重新登录");
                break;
            case R.id.relogin_tv:
                showDailog("更换账号，您需要重新登录");
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.tzjk_lock_screen_activity);
        ViewUtils.inject(this);
        if (getIntent() != null) {
            comefrom = getIntent().getStringExtra("comefrom");
        }
        //根据三个不同的解锁界面调整布局
        if ("personinfoset".equals(comefrom) || "personinforeset".equals(comefrom) || "personinfoclose".equals(comefrom)) {
            forgotpasswdoneTv.setVisibility(View.VISIBLE);
            forgotpasswdoneTv.setText("忘记手势密码");

        } else {
            forgotpasswdtwoTv.setVisibility(View.VISIBLE);
            forgotpasswdtwoTv.setText("忘记手势密码");
            reloginTv.setVisibility(View.VISIBLE);
            reloginTv.setText("用其他账号登录");
            telltitleTv.setText("请绘制手势密码解锁");
            actopenpasswdnameTv.setText("手势密码解锁");
            toolbarbackhandpasswdTv.setVisibility(View.GONE);

        }
        //如果没有绘制解锁界面则直接关闭页面
        SharedPreferences preferences = getSharedPreferences(PersonInfoActivity.LOCK,
                MODE_PRIVATE);
        String patternString = preferences.getString(PersonInfoActivity.LOCK_KEY,
                null);
        if (patternString == null) {
            finish();
            return;
        }
        lockPattern = LockPatternView.stringToPattern(patternString);
        lockPatternView.setOnPatternListener(this);


    }


    @Override
    public void onPatternStart() {

    }

    @Override
    public void onPatternCleared() {

    }

    @Override
    public void onPatternCellAdded(List<LockPatternView.Cell> pattern) {

    }

    @Override
    public void onPatternDetected(List<Cell> pattern) {
        //每个解锁界面解锁后做不同的处理
        if (pattern.equals(lockPattern)) {
            getSharedPreferences(MainActivity.COUNT, MODE_PRIVATE).edit().clear().commit();
            RequestInstance.count = 5;
//            if("personinfoset".equals(comefrom)) {
//                startActivity(new Intent(this, MainActivity.class));
//            }

            if ("personinforeset".equals(comefrom)) {
                startActivity(new Intent(this, LockSetupActivity.class));
            } else if ("personinfoclose".equals(comefrom)) {
                getSharedPreferences(PersonInfoActivity.LOCK, MODE_PRIVATE).edit().clear().commit();
            }

//            else {
//                startActivity(new Intent(this, MainActivity.class));
//
//            }
            this.finish();
        }
        //解锁错误后的处理
        else {
            lockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
            RequestInstance.count--;
            if (RequestInstance.count != 5) {
                SharedPreferences preferencesCount = getSharedPreferences(COUNT, MODE_PRIVATE);
                preferencesCount
                        .edit()
                        .putString(COUNT_KEY, (String.valueOf(RequestInstance.count)))
                        .commit();
            }
            if (RequestInstance.count == 0) {
                getSharedPreferences(PersonInfoActivity.LOCK, MODE_PRIVATE).edit().clear().commit();
                getSharedPreferences(MainActivity.COUNT, MODE_PRIVATE).edit().clear().commit();
                RequestInstance.count = 5;
                startActivity(new Intent(LockActivity.this, LoginOrRegActivity.class));
                this.finish();
            } else if (RequestInstance.count > 0) {
                telltitleTv.setText("密码错误，你还可以绘制" + String.valueOf(RequestInstance.count) + "次");
                lockPatternView.clearPattern();
            }
        }

    }

    //忘记手势密码或使用其他账号登录
    private void showDailog(String title) {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .cancelable(false)
                .customView(R.layout.tzjk_dialog_forgot_passwd, true)
                .build();

        Button cancelLayout = (Button) dialog.findViewById(R.id.dialog_forgot_passwd_cancel_button);
        Button okLayout = (Button) dialog.findViewById(R.id.dialog_forgot_passwd_ok_button);
        TextView forgotpasswddialogtitleTv = (TextView) dialog.findViewById(R.id.forgot_passwd_dialog_title);
        forgotpasswddialogtitleTv.setText(title);
        cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        okLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getSharedPreferences(PersonInfoActivity.LOCK, MODE_PRIVATE).edit().clear().commit();
                LockActivity.this.finish();
                startActivity(new Intent(LockActivity.this, LoginOrRegActivity.class));
//                UserLogout();
            }
        });
        dialog.show();
        Display d = getWindowManager().getDefaultDisplay();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = (int) (d.getWidth() * 0.9);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(params);
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && ("oncreate".equals(comefrom) || "resume".equals(comefrom))) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            this.startActivity(intent);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

}
