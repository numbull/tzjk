package com.tzjk.tzjk.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.igexin.sdk.PushConsts;
import com.igexin.sdk.PushReceiver;

/**
 * Created by cyril on 15/9/24.
 */
public class MyPushReceiver extends PushReceiver {

    /*
      应用未启动， 个推service已经被唤醒，保存在该时段内离线信息
         */
    public static StringBuilder payloadData = new StringBuilder();

    public static String cid;
    private String u;


    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();

        switch (bundle.getInt(PushConsts.CMD_ACTION)) {
            case PushConsts.GET_MSG_DATA:
                //获取透传数据
                String jsonstr = "";
                break;
            case PushConsts.GET_CLIENTID:
                //Toast.makeText(context.getApplicationContext(), "7777", Toast.LENGTH_SHORT).show();
                //获取clientid
                //第三方应用需要将CID上传到第三方服务器，并且将当前用户账号和CID进行关联，以便日后通过
                //用户账号查找CID进行消息推送
                cid = bundle.getString("clientid");

                break;
            case PushConsts.THIRDPART_FEEDBACK:

                //Toast.makeText(context.getApplicationContext(), "888", Toast.LENGTH_SHORT).show();
                break;
            default:
                //Toast.makeText(context.getApplicationContext(), "666", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
