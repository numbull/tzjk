package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/4.
 */

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.utils.ClearEditText;
import com.tzjk.tzjk.utils.MethodUtils;
import com.tzjk.tzjk.utils.ValidateUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.Timer;
import java.util.TimerTask;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class LoginActivity extends BaseActivity {

    private boolean isDisplay = false;
    @ViewInject(R.id.tzjk_login_passwd_clear_edit_text)
    private ClearEditText passwdCET;

    @ViewInject(R.id.tzjk_login_passwd_visible_image_view)
    private ImageView passwdVisibleIV;

    @ViewInject(R.id.tzjk_login_ok_button)
    private Button loginBtn;


    @OnClick({R.id.toolbar_back_button, R.id.tzjk_login_ok_button, R.id.tzjk_login_passwd_visible_image_view, R.id.tzjk_login_forget_passwd})
    private void BtnClickListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                LoginActivity.this.finish();
                break;
            case R.id.tzjk_login_ok_button:
                if (ValidateUtils.isPassword(passwdCET.getText().toString())) {
//                    Login();
                } else {
                    Toast.makeText(LoginActivity.this, "密码输入错误", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tzjk_login_forget_passwd:
                Intent intent = new Intent(LoginActivity.this, ForgetPasswdActivity.class);
                LoginActivity.this.startActivity(intent);
                break;
            case R.id.tzjk_login_passwd_visible_image_view:
                isDisplay = MethodUtils.changeVisibility(LoginActivity.this, isDisplay, passwdCET, passwdVisibleIV);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_login_activity);
        ViewUtils.inject(this);

        //自动弹出软键盘
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                        .toggleSoftInput(0,
                                InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }, 500);

        //添加文本变化监听器 判断按钮的状态
        loginBtn.setEnabled(false);
        loginBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
        passwdCET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(passwdCET.getText())) {
                    loginBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                    loginBtn.setEnabled(false);
                } else {
                    loginBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                    loginBtn.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
}
