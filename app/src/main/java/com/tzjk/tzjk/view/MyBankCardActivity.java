package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/10.
 */

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.utils.StringUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class MyBankCardActivity extends BaseActivity {
    @ViewInject(R.id.tzjk_my_bank_card_blind_layout)
    private LinearLayout blindLL;
    @ViewInject(R.id.tzjk_my_bank_card_unblind_layout)
    private LinearLayout unBlindLL;
    @ViewInject(R.id.tzjk_my_bank_card_left_layout)
    private LinearLayout blindLeftLL;
    @ViewInject(R.id.tzjk_my_bank_card_bank_name)
    private TextView bankNameTV;
    @ViewInject(R.id.tzjk_my_bank_card_bank_icon)
    private ImageView bankIconIV;
    @ViewInject(R.id.tzjk_my_bank_card_bank_account)
    private TextView bankAccountTV;
    @ViewInject(R.id.tzjk_my_bank_card_unblind_tv)
    private TextView unBlindTV;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_my_bank_card_unblind_layout, R.id.tzjk_my_bank_card_unblind_tv})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                MyBankCardActivity.this.finish();
                break;
            case R.id.tzjk_my_bank_card_unblind_layout:
                startActivity(new Intent(MyBankCardActivity.this, BlindBankCardActivity.class));
                break;
            case R.id.tzjk_my_bank_card_unblind_tv:
                showTipsDialog();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_my_bank_card_activity);
        ViewUtils.inject(this);
        //动态设置shape文件的solid颜色值
        GradientDrawable drawable = (GradientDrawable) blindLeftLL.getBackground();
        drawable.setColor(getResources().getColor(R.color.tzjk_color_orange));
        bankIconIV.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
        bankNameTV.setText("中国工商银行");
        bankAccountTV.setText(StringUtils.Remain4Lerrers("6228481232344567"));
    }

    private void showTipsDialog() {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .cancelable(false)
                .customView(R.layout.tzjk_dialog_unblind_bank_card, true)
                .build();

        LinearLayout cancelLayout = (LinearLayout) dialog.findViewById(R.id.tzjk_dialog_unblind_bank_card_cancel);
        LinearLayout okLayout = (LinearLayout) dialog.findViewById(R.id.tzjk_dialog_unblind_bank_card_ok);

        cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        okLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MyBankCardActivity.this, "去解绑银行卡", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
        Display d = getWindowManager().getDefaultDisplay();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = (int) (d.getWidth() * 0.95);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
