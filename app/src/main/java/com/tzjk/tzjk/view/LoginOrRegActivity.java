package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/4.
 */

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.model.CommonConstants;
import com.tzjk.tzjk.utils.ClearEditText;
import com.tzjk.tzjk.utils.StringUtils;
import com.tzjk.tzjk.utils.ValidateUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.Timer;
import java.util.TimerTask;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class LoginOrRegActivity extends BaseActivity {
    @ViewInject(R.id.tzjk_login_or_register_phone_clear_edit_text)
    private ClearEditText phoneNumberCET;

    @ViewInject(R.id.tzjk_login_or_register_big_number_text_view)
    private TextView bigNumberTV;

    @ViewInject(R.id.tzjk_login_or_register_ok_button)
    private Button okBtn;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_login_or_register_ok_button})
    private void btnClickListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                LoginOrRegActivity.this.finish();
                break;
            case R.id.tzjk_login_or_register_ok_button:
                if (ValidateUtils.isMobilePhone(phoneNumberCET.getText().toString())) {
                    CommonConstants.PHONE_NUMBER = phoneNumberCET.getText().toString();
                    Intent intent = new Intent(LoginOrRegActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginOrRegActivity.this, "输入的手机号码不合法", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_login_or_reg_activity);
        ViewUtils.inject(this);

        //自动弹出软键盘
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                        .toggleSoftInput(0,
                                InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }, 100);
        okBtn.setEnabled(false);
        okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
        phoneNumberCET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(phoneNumberCET.getText().toString())) {
                    okBtn.setEnabled(true);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                    bigNumberTV.setVisibility(View.VISIBLE);
                    bigNumberTV.setText(StringUtils.formatPhoneNumber(phoneNumberCET.getText().toString()));
                } else {
                    okBtn.setEnabled(false);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                    bigNumberTV.setText("");
                    bigNumberTV.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
