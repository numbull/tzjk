package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/2/21.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.utils.AutoNumberTextView;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class AssetsActivity extends BaseActivity {

    @ViewInject(R.id.tzjk_assets_user_name)
    private TextView userNameTV;
    @ViewInject(R.id.tzjk_assets_money_record_layout)
    private LinearLayout moneyRecordLL;
    @ViewInject(R.id.tzjk_assets_in_investment)
    private AutoNumberTextView investAmountTV;
    @ViewInject(R.id.tzjk_assets_uncollected_revenue)
    private TextView uncollectedAmountTV;
    @ViewInject(R.id.tzjk_assets_received_benefits)
    private TextView receivedAmountTV;
    @ViewInject(R.id.tzjk_assets_available)
    private TextView availableTV;
    @ViewInject(R.id.tzjk_assets_recharge)
    private Button rechargeBtn;
    @ViewInject(R.id.tzjk_assets_withdraw)
    private Button withdrawBtn;
    @ViewInject(R.id.tzjk_assets_my_investment)
    private LinearLayout investmentLL;
    @ViewInject(R.id.tzjk_assets_my_coupons)
    private LinearLayout couponsLL;
    @ViewInject(R.id.act_assets_coupons_count)
    private TextView couponsCountTV;
    @ViewInject(R.id.tzjk_assets_my_bank_card)
    private LinearLayout myBankCardLL;
    @ViewInject(R.id.tzjk_assets_bank_card_status)
    private TextView bankCardStatusTV;
    @ViewInject(R.id.tzjk_assets_my_point)
    private LinearLayout myPointLL;
    @ViewInject(R.id.tzjk_assets_my_point_count)
    private TextView myPointCountTV;

    @OnClick({R.id.tzjk_assets_icon_image_view, R.id.tzjk_assets_money_record_layout, R.id.tzjk_assets_my_bank_card,
            R.id.tzjk_assets_in_investment, R.id.tzjk_assets_recharge, R.id.tzjk_assets_withdraw,
            R.id.tzjk_assets_my_investment, R.id.tzjk_assets_my_coupons, R.id.tzjk_assets_my_point})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.tzjk_assets_icon_image_view:
                startActivity(new Intent(AssetsActivity.this, PersonInfoActivity.class));
                break;
            case R.id.tzjk_assets_money_record_layout:
                startActivity(new Intent(AssetsActivity.this, MoneyRecordActivity.class));
                break;
            case R.id.tzjk_assets_my_bank_card:
                startActivity(new Intent(AssetsActivity.this, MyBankCardActivity.class));
                break;
            case R.id.tzjk_assets_in_investment:
                startActivity(new Intent(AssetsActivity.this, MyInvestmentRecordActivity.class));
                break;
            case R.id.tzjk_assets_recharge:
                break;
            case R.id.tzjk_assets_withdraw:
                break;
            case R.id.tzjk_assets_my_investment:
                startActivity(new Intent(AssetsActivity.this, MyInvestmentRecordActivity.class));
                break;
            case R.id.tzjk_assets_my_coupons:
                startActivity(new Intent(AssetsActivity.this, MyCouponsActivity.class));
                break;
            case R.id.tzjk_assets_my_point:
                startActivity(new Intent(AssetsActivity.this, MyPointActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_assets_activity);
        ViewUtils.inject(this);
    }
}
