package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/11.
 */

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class MyCouponsActivity extends BaseActivity {

    private List<Fragment> fragmentslist;
    private CouponsAvailableFrag couponsAvailableFrag;
    private CouponsUsedFrag couponsUsedFrag;
    private CouponsExpiredFrag couponsExpiredFrag;

    @ViewInject(R.id.tzjk_my_coupons_available)
    private LinearLayout availableLL;
    @ViewInject(R.id.tzjk_my_coupons_available_tv)
    private TextView availableTV;
    @ViewInject(R.id.tzjk_my_coupons_available_tag)
    private View availableView;

    @ViewInject(R.id.tzjk_my_coupons_used)
    private LinearLayout usedLL;
    @ViewInject(R.id.tzjk_my_coupons_used_tv)
    private TextView usedTV;
    @ViewInject(R.id.tzjk_my_coupons_used_tag)
    private View usedView;

    @ViewInject(R.id.tzjk_my_coupons_expired)
    private LinearLayout expiredLL;
    @ViewInject(R.id.tzjk_my_coupons_expired_tv)
    private TextView expiredTV;
    @ViewInject(R.id.tzjk_my_coupons_expired_tag)
    private View expiredView;

    @ViewInject(R.id.tzjk_my_coupons_viewpager)
    private ViewPager viewPager;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_my_coupons_available, R.id.tzjk_my_coupons_expired, R.id.tzjk_my_coupons_used, R.id.tzjk_my_coupons_question_mark})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                MyCouponsActivity.this.finish();
                break;
            case R.id.tzjk_my_coupons_available:
                changeTab(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.tzjk_my_coupons_expired:
                changeTab(2);
                viewPager.setCurrentItem(2);
                break;
            case R.id.tzjk_my_coupons_used:
                changeTab(1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.tzjk_my_coupons_question_mark:
                Intent intent = new Intent(MyCouponsActivity.this, H5WebViewActivity.class);
                intent.putExtra("title", "优惠券说明");
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_my_coupons_activity);
        ViewUtils.inject(this);


        fragmentslist = new ArrayList<>();
        fragmentslist.add(couponsAvailableFrag);
        fragmentslist.add(couponsUsedFrag);
        fragmentslist.add(couponsExpiredFrag);
        availableTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
        availableView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(new CouponsAdapter(getSupportFragmentManager(), fragmentslist));
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        changeTab(0);
                        break;
                    case 1:
                        changeTab(1);
                        break;
                    case 2:
                        changeTab(2);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    private void changeTab(int position) {
        switch (position) {
            case 0:
                availableTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
                usedTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                expiredTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                availableView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
                usedView.setBackgroundColor(Color.WHITE);
                expiredView.setBackgroundColor(Color.WHITE);
                viewPager.setCurrentItem(0);

                break;
            case 1:
                availableTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                usedTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
                expiredTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                availableView.setBackgroundColor(Color.WHITE);
                usedView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
                expiredView.setBackgroundColor(Color.WHITE);
                viewPager.setCurrentItem(1);
                break;
            case 2:
                availableTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                usedTV.setTextColor(getResources().getColor(R.color.tzjk_text_gray7));
                expiredTV.setTextColor(getResources().getColor(R.color.tzjk_color_orange));
                availableView.setBackgroundColor(Color.WHITE);
                usedView.setBackgroundColor(Color.WHITE);
                expiredView.setBackgroundColor(getResources().getColor(R.color.tzjk_color_orange));
                viewPager.setCurrentItem(2);
                break;
        }
    }

    class CouponsAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentList;

        public CouponsAdapter(FragmentManager fm, List<Fragment> list) {
            super(fm);
            this.fragmentList = list;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                if (couponsAvailableFrag == null) {
                    couponsAvailableFrag = couponsAvailableFrag.newInstance();
                }
                return couponsAvailableFrag;
            } else if (position == 1) {
                if (couponsUsedFrag == null) {
                    couponsUsedFrag = couponsUsedFrag.newInstance();
                }
                return couponsUsedFrag;
            } else if (position == 2) {
                if (couponsExpiredFrag == null) {
                    couponsExpiredFrag = couponsExpiredFrag.newInstance();
                }
                return couponsExpiredFrag;
            }
            return null;
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}

