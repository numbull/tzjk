package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/2/18.
 */

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.Toast;

import com.igexin.sdk.PushManager;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.tzjk.tzjk.R;
import com.umeng.analytics.AnalyticsConfig;
import com.umeng.analytics.MobclickAgent;

import java.util.Timer;
import java.util.TimerTask;

import cn.sharesdk.framework.ShareSDK;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class MainActivity extends TabActivity {
    private static TabHost tabHost;
    private Intent intent;
    public static Activity mainActivity;
    public static final String COUNT = "count";
    public static final String COUNT_KEY = "count_key";
    @ViewInject(R.id.main_tab_btn_0)
    private static RadioButton discovery_tab_btn;
    @ViewInject(R.id.main_tab_btn_1)
    private static RadioButton lccp_tab_btn;
    @ViewInject(R.id.main_tab_btn_2)
    private static RadioButton assets_tab_btn;
    @ViewInject(R.id.main_tab_btn_3)
    private static RadioButton more_tab_btn;
    @ViewInject(R.id.main_tab_group)
    private RadioGroup radioGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_main_activity);
        ViewUtils.inject(this);
        mainActivity = this;
        //友盟配置
        AnalyticsConfig.enableEncrypt(true);
        MobclickAgent.openActivityDurationTrack(false);

        //初始化SDK
        PushManager.getInstance().initialize(this.getApplicationContext());
        //ShareSDK初始化
        ShareSDK.initSDK(this);

        tabHost = this.getTabHost();
        TabHost.TabSpec spec;
        intent = new Intent(this, DiscoveryActivity.class);
        spec = tabHost.newTabSpec(getResources().getString(R.string.act_main_tab_text_0))
                .setIndicator(getResources().getString(R.string.act_main_tab_text_0))
                .setContent(intent);
        tabHost.addTab(spec);


        intent = new Intent(this, FinancialProductActivity.class);
        spec = tabHost.newTabSpec(getResources().getString(R.string.act_main_tab_text_1))
                .setIndicator(getResources().getString(R.string.act_main_tab_text_1))
                .setContent(intent);
        tabHost.addTab(spec);


        intent = new Intent(this, AssetsActivity.class);
        spec = tabHost.newTabSpec(getResources().getString(R.string.act_main_tab_text_2))
                .setIndicator(getResources().getString(R.string.act_main_tab_text_2))
                .setContent(intent);
        tabHost.addTab(spec);


        intent = new Intent(this, MoreActivity.class);
        spec = tabHost.newTabSpec(getResources().getString(R.string.act_main_tab_text_3))
                .setIndicator(getResources().getString(R.string.act_main_tab_text_3))
                .setContent(intent);
        tabHost.addTab(spec);

        //设置颜色

        //设置当前选中的Tab
        tabHost.setCurrentTab(0);
        discovery_tab_btn.setChecked(true);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.main_tab_btn_0:

                        tabHost.setCurrentTabByTag(getResources()
                                .getString(R.string.act_main_tab_text_0));
                        break;
                    case R.id.main_tab_btn_1:
                        tabHost.setCurrentTabByTag(getResources()
                                .getString(R.string.act_main_tab_text_1));
                        break;
                    case R.id.main_tab_btn_2:

                        tabHost.setCurrentTabByTag(getResources()
                                .getString(R.string.act_main_tab_text_2));
                        break;
                    case R.id.main_tab_btn_3:
                        tabHost.setCurrentTabByTag(getResources()
                                .getString(R.string.act_main_tab_text_3));
                        break;
                    default:
                        break;
                }
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public static void ChangeTab(int index) {
        switch (index) {
            case 0:
                discovery_tab_btn.setChecked(true);
                tabHost.setCurrentTab(0);
                break;
            case 1:
                lccp_tab_btn.setChecked(true);
                tabHost.setCurrentTab(1);
                break;
            case 2:
                assets_tab_btn.setChecked(true);
                tabHost.setCurrentTab(2);
                break;
            case 3:
                more_tab_btn.setChecked(true);
                tabHost.setCurrentTab(3);
                break;
            default:
                break;
        }
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            exitByDoubleClick();
            return false;
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * 双击退出函数
     */
    private static Boolean isExit = false;

    private void exitByDoubleClick() {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true;
            Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false;
                }
            }, 2000);
        } else {
            MobclickAgent.onKillProcess(MainActivity.this);
            MainActivity.this.finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

}
