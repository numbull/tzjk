package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/9.
 */

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.utils.ClearEditText;
import com.tzjk.tzjk.utils.MethodUtils;
import com.tzjk.tzjk.utils.ValidateUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class ModifyLoginPasswdActivity extends BaseActivity {
    private boolean isDisplay;

    @ViewInject(R.id.tzjk_modify_login_passwd_old_password_cet)
    private ClearEditText oldPasswdCET;
    @ViewInject(R.id.tzjk_modify_login_passwd_new_passwd_cet)
    private ClearEditText newPasswdCET;
    @ViewInject(R.id.tzjk_modify_login_passwd_visible_button_ll)
    private LinearLayout visibleBtnLL;
    @ViewInject(R.id.tzjk_modify_login_passwd_visible_button_iv)
    private ImageView visibleBtnIV;
    @ViewInject(R.id.tzjk_modify_login_passwd_ok_button)
    private Button okBtn;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_modify_login_passwd_visible_button_ll, R.id.tzjk_modify_login_passwd_ok_button})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                ModifyLoginPasswdActivity.this.finish();
                break;
            case R.id.tzjk_modify_login_passwd_visible_button_ll:
                isDisplay = MethodUtils.changeVisibility(ModifyLoginPasswdActivity.this, isDisplay, newPasswdCET, visibleBtnIV);
                break;
            case R.id.tzjk_modify_login_passwd_ok_button:
                if (!ValidateUtils.isPassword(oldPasswdCET.getText().toString())) {
                    Toast.makeText(ModifyLoginPasswdActivity.this, "原密码格式不合法", Toast.LENGTH_SHORT).show();
                } else if (!ValidateUtils.isPassword(newPasswdCET.getText().toString())) {
                    Toast.makeText(ModifyLoginPasswdActivity.this, "新密码格式不合法", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ModifyLoginPasswdActivity.this, "修改请求以发出", Toast.LENGTH_SHORT).show();
//                    DoModify();
                }
                break;
            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_modify_login_passwd_activity);
        ViewUtils.inject(this);

        okBtn.setEnabled(false);
        okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
        oldPasswdCET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(oldPasswdCET.getText().toString()) || TextUtils.isEmpty(newPasswdCET.getText().toString())
                        ) {
                    okBtn.setEnabled(false);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                } else {
                    okBtn.setEnabled(true);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        newPasswdCET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(oldPasswdCET.getText().toString()) || TextUtils.isEmpty(newPasswdCET.getText().toString())) {
                    okBtn.setEnabled(false);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_gray_disable_btn_shape));
                } else {
                    okBtn.setEnabled(true);
                    okBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_corner_orange_enable_btn_shape));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
}
