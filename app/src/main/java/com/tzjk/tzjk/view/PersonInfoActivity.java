package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/10.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tzjk.tzjk.R;
import com.umeng.analytics.MobclickAgent;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class PersonInfoActivity extends BaseActivity {
    private int count;
    private String lockPattenString;
    public static final String LOCK = "lock";
    public static final String LOCK_KEY = "lock_key";
    @ViewInject(R.id.tzjk_personal_info_account_number)
    private TextView acountNumberTV;
    @ViewInject(R.id.tzjk_personal_info_user_name)
    private TextView userNameTV;
    @ViewInject(R.id.tzjk_personal_info_id_card)
    private TextView IdCardTV;
    @ViewInject(R.id.tzjk_personal_info_pay_account)
    private LinearLayout payAccountLL;
    @ViewInject(R.id.tzjk_personal_info_open_pay_account_btn)
    private Button openPayBtn;
    @ViewInject(R.id.tzjk_personal_info_modify_login_passwd)
    private LinearLayout modLoginPasswdLL;
    @ViewInject(R.id.tzjk_personal_info_modify_exchange_passwd)
    private LinearLayout modExchangePasswdLL;
    @ViewInject(R.id.tzjk_open_gesture_passwd_btn)
    private ToggleButton gesturePWDBtn;
    @ViewInject(R.id.tzjk_personal_info_reset_gesture_passwd)
    private LinearLayout resetGesturePasswdLL;
    @ViewInject(R.id.tzjk_personal_info_logout_button)
    private Button logoutBtn;

    @OnClick({R.id.toolbar_back_button, R.id.tzjk_personal_info_open_pay_account_btn, R.id.tzjk_personal_info_modify_login_passwd,
            R.id.tzjk_personal_info_modify_exchange_passwd, R.id.tzjk_open_gesture_passwd_btn, R.id.tzjk_personal_info_reset_gesture_passwd,
            R.id.tzjk_personal_info_logout_button})
    private void clickEventListener(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back_button:
                PersonInfoActivity.this.finish();
                break;
            case R.id.tzjk_personal_info_open_pay_account_btn:
                Toast.makeText(PersonInfoActivity.this, "开通三方支付", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tzjk_personal_info_modify_login_passwd:
                startActivity(new Intent(PersonInfoActivity.this, ModifyLoginPasswdActivity.class));
                PersonInfoActivity.this.finish();
                break;
            case R.id.tzjk_personal_info_modify_exchange_passwd:
                Toast.makeText(PersonInfoActivity.this, "修改交易密码", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tzjk_open_gesture_passwd_btn:
                if (count == 0) {
                    Intent intent = new Intent(PersonInfoActivity.this, LockSetupActivity.class);
                    intent.putExtra("openpasswd", "one");
                    startActivity(intent);
                    if (lockPattenString != null) {
                        gesturePWDBtn.setChecked(true);
                        resetGesturePasswdLL.setVisibility(View.VISIBLE);
                    } else {
                        gesturePWDBtn.setChecked(false);
                        resetGesturePasswdLL.setVisibility(View.GONE);
                    }
                    count = 1;
                } else {
                    gesturePWDBtn.setChecked(false);
                    Intent intent = new Intent(PersonInfoActivity.this, LockActivity.class);
                    intent.putExtra("comefrom", "personinfoclose");
                    startActivity(intent);
                    count = 0;
                }
                break;
            case R.id.tzjk_personal_info_reset_gesture_passwd:
                Toast.makeText(PersonInfoActivity.this, "重置手势密码", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tzjk_personal_info_logout_button:
                Toast.makeText(PersonInfoActivity.this, "退出登录", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tzjk_personal_info_activity);
        ViewUtils.inject(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);

        //判断是否设置了手势密码调整布局
        SharedPreferences preferences = getSharedPreferences(LOCK, MODE_PRIVATE);
        lockPattenString = preferences.getString(LOCK_KEY, null);
        if (lockPattenString != null) {
            resetGesturePasswdLL.setVisibility(View.VISIBLE);
            count = 1;
            gesturePWDBtn.setChecked(true);
        } else {
            count = 0;
            resetGesturePasswdLL.setVisibility(View.GONE);
            gesturePWDBtn.setChecked(false);
        }
    }
}
