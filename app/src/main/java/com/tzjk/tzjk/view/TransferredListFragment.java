package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/2/23.
 */

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.tzjk.tzjk.R;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class TransferredListFragment extends Fragment {
    @ViewInject(R.id.ptrl_transferred_list)
    private PullToRefreshListView pullToRefreshListView;

    private View rootView;
    private ListView listView;
    private View emptyView;

    public TransferredListFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tzjk_transferred_list_fragment, container, false);
        ViewUtils.inject(this, rootView);

        emptyView = View.inflate(getActivity(), R.layout.tzjk_ptrl_bg_no_record_layout, null);
        listView = pullToRefreshListView.getRefreshableView();
        listView.setEmptyView(emptyView);
        pullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
        return rootView;
    }
}
