package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/11.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.model.InvestmentHoldingMod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class HoldingFragment extends Fragment implements View.OnTouchListener {

    private View rootView;
    private View emptyView;
    private ListView listView;
    private PullToRefreshListView pullToRefreshListView;

    private HoldingAdapter adapter;
    private List<InvestmentHoldingMod> holdingList;

    public static HoldingFragment newInstance() {
        HoldingFragment holdingFragment = new HoldingFragment();
        return holdingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.tzjk_investment_record_holding_fragment, container, false);
            emptyView = inflater.inflate(R.layout.tzjk_ptrl_bg_no_record_layout, null);

            pullToRefreshListView = (PullToRefreshListView) rootView.findViewById(R.id.tzjk_investment_record_holding_listview);
            listView = pullToRefreshListView.getRefreshableView();
            pullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
            pullToRefreshListView.setEmptyView(emptyView);
            listView.setDivider(null);
            listView.setDividerHeight(20);

            holdingList = new ArrayList<>();
            InvestmentHoldingMod holding1 = new InvestmentHoldingMod();
            holding1.setStatus("0");
            holdingList.add(holding1);
            InvestmentHoldingMod holding2 = new InvestmentHoldingMod();
            holding2.setStatus("1");
            holdingList.add(holding2);
            InvestmentHoldingMod holding3 = new InvestmentHoldingMod();
            holding3.setStatus("2");
            holdingList.add(holding3);


            adapter = new HoldingAdapter(rootView.getContext(), 0, holdingList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    /*
                    *该注释 只用于测试与正式数据无关
                    *0=原项目 1=未支付 2=可转让
                    *
                     */
                    Intent intent = new Intent(getActivity(), InvestDetailForHoldingAct.class);
                    if ("0".equals(holdingList.get(position).getStatus())) {
                        intent.putExtra("status", "0");
                    } else if ("1".equals(holdingList.get(position).getStatus())) {
                        intent.putExtra("status", "1");
                    } else if ("2".equals(holdingList.get(position).getStatus())) {
                        intent.putExtra("status", "2");
                    } else {
                    }

                    startActivity(intent);
                }
            });
        }

        return rootView;
    }


    class HoldingAdapter extends ArrayAdapter<InvestmentHoldingMod> {
        private List<InvestmentHoldingMod> lists;
        private HashMap<Integer, View> lmap = new HashMap<Integer, View>();
        private List<Map<String, String>> mData = null;
        private Context context;
        private LayoutInflater mInflater = null;

        public List<Map<String, String>> getmData() {
            return mData;
        }

        public HoldingAdapter(Context context, int resource, List<InvestmentHoldingMod> lists) {
            super(context, resource, lists);
            this.lists = lists;
            this.mInflater = LayoutInflater.from(context);
            this.context = context;
        }

        public void setmData(List<Map<String, String>> mData) {
            this.mData = mData;
        }

        @Override
        public int getCount() {
            if (lists == null) {
                return 0;
            }
            return lists.size();
        }

        @Override
        public InvestmentHoldingMod getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = View.inflate(getActivity(), R.layout.tzjk_item_investment_holding, null);
                holder.nameTV = (TextView) convertView.findViewById(R.id.tzjk_item_investment_holding_project_name);
                holder.statusTV = (TextView) convertView.findViewById(R.id.tzjk_item_investment_holding_status);
                holder.holdamountTV = (TextView) convertView.findViewById(R.id.tzjk_item_investment_holding_price);
                holder.payDateTV = (TextView) convertView.findViewById(R.id.tzjk_item_investment_holding_start_time);
                holder.endDateTV = (TextView) convertView.findViewById(R.id.tzjk_item_investment_holding_end_time);
                holder.incomeTV = (TextView) convertView.findViewById(R.id.tzjk_item_investment_holding_value);
                holder.payBtn = (Button) convertView.findViewById(R.id.tzjk_item_investment_holding_pay_button);
                holder.transferBtn = (Button) convertView.findViewById(R.id.tzjk_item_investment_holding_transfer_button);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if ("0".equals(lists.get(position).getStatus())) {
                holder.statusTV.setVisibility(View.VISIBLE);
                holder.payBtn.setVisibility(View.GONE);
                holder.transferBtn.setVisibility(View.GONE);
            } else if ("1".equals(lists.get(position).getStatus())) {
                holder.statusTV.setVisibility(View.GONE);
                holder.payBtn.setVisibility(View.VISIBLE);
                holder.transferBtn.setVisibility(View.GONE);
                holder.payBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), "去支付", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if ("2".equals(lists.get(position).getStatus())) {
                holder.statusTV.setVisibility(View.GONE);
                holder.payBtn.setVisibility(View.GONE);
                holder.transferBtn.setVisibility(View.VISIBLE);
                holder.transferBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), "我要转让", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            return convertView;
        }


        class ViewHolder {
            TextView nameTV;
            TextView statusTV;
            TextView holdamountTV;
            TextView payDateTV;
            TextView endDateTV;
            TextView incomeTV;
            Button payBtn;
            Button transferBtn;
        }
    }

    //抢焦点
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v instanceof Button) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                v.setFocusable(false);
                v.setFocusableInTouchMode(false);
            } else {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
            }
        } else {
            v.setFocusable(false);
            v.setFocusableInTouchMode(false);
        }
        return false;
    }
}
