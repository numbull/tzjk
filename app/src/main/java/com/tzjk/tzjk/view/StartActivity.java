package com.tzjk.tzjk.view;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.igexin.sdk.PushManager;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.utils.SharedPreferenceshelper;
import com.tzjk.tzjk.utils.SpringProgressView;
import com.tzjk.tzjk.utils.ValidateUtils;
import com.tzjk.tzjk.http.HttpGet;
import com.umeng.analytics.AnalyticsConfig;
import com.umeng.analytics.MobclickAgent;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import cn.sharesdk.framework.ShareSDK;


public class StartActivity extends BaseActivity {


    private String versionName;
    private long fileSize;
    private int downLoadFileSize;
    private MaterialDialog dialog;
    private SpringProgressView springProgressView;
    private final String PACKAGE_NAME = "Tzjk.apk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.tzjk_start_activity);
        //友盟配置
        AnalyticsConfig.enableEncrypt(true);
        MobclickAgent.openActivityDurationTrack(false);
        //初始化SDK
        PushManager.getInstance().initialize(this.getApplicationContext());
        //ShareSDK初始化
        ShareSDK.initSDK(this);

        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo("com.tzjk.tzjk", PackageManager.GET_CONFIGURATIONS);
            versionName = pinfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        Goin();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MobclickAgent.onKillProcess(StartActivity.this);
        StartActivity.this.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void Goin() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!versionName.equals(SharedPreferenceshelper.ReadSharedPreferences(StartActivity.this, "VERSION_NAME"))) {
                    SharedPreferenceshelper.SaveSharedPreferences(StartActivity.this, "VERSION_NAME", versionName);
                    StartActivity.this.startActivity(new Intent(StartActivity.this, WelcomeActivity.class));
                    StartActivity.this.finish();
                } else {
                    StartActivity.this.startActivity(new Intent(StartActivity.this, MainActivity.class));
                    StartActivity.this.finish();
                }
            }
        }, 2000);
    }

    private void downloadDialog() {
        dialog = new MaterialDialog.Builder(StartActivity.this)
                .theme(Theme.LIGHT)
                .customView(R.layout.tzjk_dialog_new_version_download, true)
                .cancelable(false)
                .build();
        TextView content = (TextView) dialog.findViewById(R.id.tzjk_dialog_new_version_download_content);
        final Button updateBtn = (Button) dialog.findViewById(R.id.tzjk_dialog_new_version_download_button);
        final ImageView cancelBtn = (ImageView) dialog.findViewById(R.id.tzjk_dialog_new_version_download_dismiss_button);
        springProgressView = (SpringProgressView) dialog.findViewById(R.id.tzjk_dialog_new_version_download_progress_bar);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ValidateUtils.isNetworkAvailable(StartActivity.this)) {
                    Toast.makeText(StartActivity.this, "当前网络不可用", Toast.LENGTH_SHORT).show();
                } else {
                    cancelBtn.setEnabled(false);
                    updateBtn.setVisibility(View.GONE);
                    springProgressView.setVisibility(View.VISIBLE);
//                    downFile(response.getUrl());
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(params);
    }

    //进度条刷新
    private Handler updateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (!Thread.currentThread().isInterrupted()) {
                switch (msg.what) {
                    case 0:
                        //设置进度条最大值
                        springProgressView.setMaxCount(fileSize);
                        break;
                    case 1:
                        springProgressView.setCurrentCount(downLoadFileSize);
                        //设置进度条当前进度
                        break;
                    case 2:
                        //下载完成
                        dialog.dismiss();
                        install();
                        break;
                }
            }

            super.handleMessage(msg);
        }
    };

    //下载安装包
    private void downFile(final String url) {
        new Thread() {
            @Override
            public void run() {
                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(url);
                HttpResponse response;
                try {
                    response = client.execute(get);
                    HttpEntity entity = response.getEntity();
                    fileSize = entity.getContentLength();
                    InputStream is = entity.getContent();
                    FileOutputStream fileOutputStream = null;
                    if (is != null) {
                        File file = new File(Environment.getExternalStorageDirectory(), PACKAGE_NAME);
                        fileOutputStream = new FileOutputStream(file);
                        byte[] buf = new byte[1024];
                        int ch = -1;
                        downLoadFileSize = 0;
                        sendMsg(0);
                        while ((ch = is.read(buf)) != -1) {
                            fileOutputStream.write(buf, 0, ch);
                            downLoadFileSize += ch;
                            sendMsg(1);//更新下载进度
                        }
                    }
                    fileOutputStream.flush();
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                    sendMsg(2);//通知下载完成
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void sendMsg(int flag) {
        Message msg = new Message();
        msg.what = flag;
        updateHandler.sendMessage(msg);
    }

    //安装下载的应用包
    private void install() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory(), PACKAGE_NAME)),
                "application/vnd.android.package-archive");
        startActivity(intent);
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
