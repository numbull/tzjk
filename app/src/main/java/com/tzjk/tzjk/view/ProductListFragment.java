package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/2/21.
 */


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.tzjk.tzjk.R;
import com.tzjk.tzjk.model.ProjectMod;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class ProductListFragment extends Fragment {
    @ViewInject(R.id.ptrl_product_list)
    private PullToRefreshListView pullToRefreshListView;

    private View rootView;
    private ListView listView;
    private View emptyView;
    private View headerView;

    private ProjectAdapter adapter;
    private int counts = 0;
    private EventBus eventBus = EventBus.getDefault();

    public ProductListFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tzjk_product_list_fragment, container, false);
        ViewUtils.inject(this, rootView);
        emptyView = View.inflate(getActivity(), R.layout.tzjk_ptrl_bg_no_record_layout, null);
        headerView = View.inflate(getActivity(), R.layout.tzjk_item_particular_project, null);
        listView = pullToRefreshListView.getRefreshableView();
        listView.setEmptyView(emptyView);
        listView.addHeaderView(headerView);
        listView.setDivider(null);
        listView.setDividerHeight(20);
        pullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);

        /*
        *0=新手 1=热销 2=预告 3=已抢光 4=已售罄 5=已结清
         */
        final List<ProjectMod> projectList = new ArrayList<>();
        ProjectMod project = new ProjectMod();
        project.setProName("涌金新手");
        project.setStatus("0");
        projectList.add(project);

        ProjectMod project1 = new ProjectMod();
        project1.setProName("余姚民生");
        project1.setStatus("1");
        projectList.add(project1);

        ProjectMod project2 = new ProjectMod();
        project2.setProName("涌金新手");
        project2.setStatus("2");
        projectList.add(project2);

        ProjectMod project3 = new ProjectMod();
        project3.setProName("长兴01");
        project3.setStatus("3");
        projectList.add(project3);

        ProjectMod project4 = new ProjectMod();
        project4.setProName("长兴02");
        project4.setStatus("4");
        projectList.add(project4);

        ProjectMod project5 = new ProjectMod();
        project5.setProName("长兴03");
        project5.setStatus("5");
        projectList.add(project5);

        adapter = new ProjectAdapter(getActivity(), 0, projectList);

        for (int i = 0; i < projectList.size(); i++) {
            if (("3".equals(projectList.get(i).getStatus()) ||
                    "4".equals(projectList.get(i).getStatus()) ||
                    "5".equals(projectList.get(i).getStatus())
            ) && counts == 0) {
                adapter.addSeparatorItemAt(i, projectList.get(i));
                counts = 1;
            }
        }
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    eventBus.post("EXCLUSIVE");
                } else {
                    Intent intent = new Intent(getActivity(), ProjectDetailActvitiy.class);
                    intent.putExtra("proName", projectList.get(position - 1).getProName());
                    startActivity(intent);
                }
            }
        });
        eventBus.register(getActivity());
        return rootView;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        eventBus.unregister(getActivity());
    }

    class ProjectAdapter extends ArrayAdapter<ProjectMod> {
        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;
        private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;
        private List<ProjectMod> list;
        private TreeSet<Integer> mSeparatorsSet = new TreeSet<Integer>();


        public ProjectAdapter(Context context, int resource, List<ProjectMod> list) {
            super(context, resource, list);
            this.list = list;
        }


        public void addSeparatorItem(final ProjectMod item) {

            list.add(item);
            //list.
            //保存位置
            mSeparatorsSet.add(list.size() - 1);
            list.add(item);
            notifyDataSetChanged();
        }

        public void addSeparatorItemAt(int index, final ProjectMod item) {

            list.add(index, item);
            //list.
            //保存位置
            mSeparatorsSet.add(index);
            notifyDataSetChanged();
        }

        @Override
        public ProjectMod getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public boolean isEnabled(int position) {
            if (getItemViewType(position) == TYPE_SEPARATOR) {
                return false;
            }
            return super.isEnabled(position);
        }

        @Override
        public int getItemViewType(int position) {
            return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            ViewHolder holder = null;
            SeperatorViewHolder seperatorViewHolder = null;
            int type = getItemViewType(position);
            if (convertView == null) {
                switch (type) {
                    case TYPE_ITEM:
                        holder = new ViewHolder();
                        convertView = View.inflate(getContext(), R.layout.tzjk_item_project, null);
                        //项目类型
                        holder.proNameTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_name);
                        holder.proUnitTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_investment_amount_unit);
                        holder.proRateOneTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_rate_one);
                        holder.proRateTwoTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_rate_two);
                        holder.proPercentOneTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_percent_one);
                        holder.proPercentTwoTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_percent_two);
                        holder.proPlusSignTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_plus_sign);
                        holder.proTimeTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_time);
                        holder.proInvestmentAmountTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_investment_amount);
                        holder.proTimeDescTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_time_desc);
                        holder.proAmountDescTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_investment_amount_desc);
                        holder.proRateDescTV = (TextView) convertView.findViewById(R.id.tzjk_item_project_rate_desc);
                        holder.proStatusIV = (ImageView) convertView.findViewById(R.id.tzjk_item_project_status_imageview);

                        convertView.setTag(holder);

                        break;
                    case TYPE_SEPARATOR:

                        seperatorViewHolder = new SeperatorViewHolder();
                        convertView = View.inflate(getContext(), R.layout.tzjk_item_seperator, null);
                        seperatorViewHolder.titleTV = (TextView) convertView.findViewById(R.id.tzjk_item_seperator_title);
                        convertView.setTag(seperatorViewHolder);
                        break;
                }


            } else {

                switch (type) {
                    case TYPE_ITEM:
                        holder = (ViewHolder) convertView.getTag();
                        holder.proStatusIV.setBackgroundDrawable(null);
                        break;
                    case TYPE_SEPARATOR:
                        seperatorViewHolder = (SeperatorViewHolder) convertView.getTag();
                        break;
                }
            }
            switch (type) {
                case TYPE_ITEM:
                    holder.proNameTV.setText(list.get(position).getProName());
                    if ("0".equals(list.get(position).getStatus())) {
                        holder.proStatusIV.setBackgroundDrawable(getResources().getDrawable(R.drawable.tzjk_icon_project_new));
                    } else if ("1".equals(list.get(position).getStatus())) {
                        holder.proStatusIV.setBackgroundDrawable(getResources().getDrawable(R.drawable.tzjk_icon_project_hot));
                    } else if ("2".equals(list.get(position).getStatus())) {
                        holder.proStatusIV.setBackgroundDrawable(getResources().getDrawable(R.drawable.tzjk_icon_project_yugao));
                    } else if ("3".equals(list.get(position).getStatus())) {
                        holder.proStatusIV.setBackgroundDrawable(getResources().getDrawable(R.drawable.tzjk_icon_project_end));
                    } else if ("4".equals(list.get(position).getStatus())) {
                        holder.proStatusIV.setBackgroundDrawable(getResources().getDrawable(R.drawable.tzjk_icon_project_end));
                    } else if ("5".equals(list.get(position).getStatus())) {
                        holder.proStatusIV.setBackgroundDrawable(getResources().getDrawable(R.drawable.tzjk_icon_project_end));
                    } else {

                    }

                    break;

                case TYPE_SEPARATOR:
                    seperatorViewHolder.titleTV.setText("已结束");
                    break;
            }

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        class ViewHolder {
            private TextView proNameTV;//显示项目的名称
            private TextView proUnitTV;//单位文本
            private TextView proRateOneTV;//显示预期年化收益率
            private TextView proRateTwoTV;//显示预期年化收益率相加的部分
            private TextView proPercentOneTV;//第一个年化收益率后的 %
            private TextView proPlusSignTV;//加号 +
            private TextView proPercentTwoTV;//第二个年化收益率后的 %
            private TextView proTimeTV;//投资期限
            private TextView proTimeUnitTV;
            private TextView proInvestmentAmountTV; //起投金额
            private TextView proTimeDescTV;//显示不同产品 关于投资期限的描述
            private TextView proStatusTV;
            private LinearLayout list_detail_view;
            private TextView proRateDescTV;
            private ImageView proStatusIV;//状态
            private TextView proAmountDescTV;
        }


        class SeperatorViewHolder {
            private TextView titleTV;//显示项目的类型
        }


    }

}
