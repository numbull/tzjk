package com.tzjk.tzjk.view;/**
 * Created by cyril on 16/3/11.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.tzjk.tzjk.R;

/**
 * ----------Dragon be here!----------/
 * ***┏┓******┏┓*********
 * *┏━┛┻━━━━━━┛┻━━┓*******
 * *┃             ┃*******
 * *┃     ━━━     ┃*******
 * *┃             ┃*******
 * *┃  ━┳┛   ┗┳━  ┃*******
 * *┃             ┃*******
 * *┃     ━┻━     ┃*******
 * *┃             ┃*******
 * *┗━━━┓     ┏━━━┛*******
 * *****┃     ┃神兽保佑*****
 * *****┃     ┃代码无BUG！***
 * *****┃     ┗━━━━━━━━┓*****
 * *****┃              ┣┓****
 * *****┃              ┏┛****
 * *****┗━┓┓┏━━━━┳┓┏━━━┛*****
 * *******┃┫┫****┃┫┫********
 * *******┗┻┛****┗┻┛*********
 * ━━━━━━神兽出没━━━━━━by:wangziren
 */


public class CouponsUsedFrag extends Fragment {

    private View rootView;
    private View emptyView;
    private ListView listView;
    private PullToRefreshListView pullToRefreshListView;


    public static CouponsUsedFrag newInstance() {
        CouponsUsedFrag couponsUsedFrag = new CouponsUsedFrag();
        return couponsUsedFrag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.tzjk_coupons_used_fragment, container, false);
            emptyView = inflater.inflate(R.layout.tzjk_ptrl_bg_no_record_layout, null);

            pullToRefreshListView = (PullToRefreshListView) rootView.findViewById(R.id.tzjk_coupons_used_listview);
            listView = pullToRefreshListView.getRefreshableView();
            pullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
            pullToRefreshListView.setEmptyView(emptyView);

        }
        return rootView;
    }
}
